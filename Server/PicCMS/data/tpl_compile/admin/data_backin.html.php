<?php
 /* compiled by (WeePHP) at (2014-08-14 15:31:26) */

 $this->display('header.html');?>
<form action="?c=Data&a=del" method="post" id="myform" name="myform">
<table width="98%" border="0" cellpadding="4" cellspacing="1" class="table">

<tr class="table_title"><td colspan="6">数据库分卷还原</td></tr>
<tr class="list_head ct">
<td width="60">编号</td>
<td >文件名</td>
<td width="50">卷号</td>
<td width="60">文件大小</td>
<td width="150">备份时间</td>
<td width="120">操作</td>
</tr>
<?php foreach($this->data['sqlFile'] as $this->data['key'] => $this->data['val']){?>
<tr class="tr">
  <td class="lt"><input type="checkbox" name="ids[]" value="<?php echo $this->data['val']['filename'];?>" class="noborder"><?php echo $this->data['key']+1;?></td>
  <td class="lt"><?php echo $this->data['val']['filename'];?></td>
  <td class="td ct"><?php echo $this->data['val']['page'];?></td>
  <td class="td ct"><?php echo $this->data['val']['filesize'];?></td>
  <td class="td ct"><?php echo $this->data['val']['maketime'];?></td>
  <td class="td ct">
  <a href="?c=Data&a=import&id=<?php echo $this->data['val']['pre'];?>" onClick="return confirm('导入数据会删除现在数据库的所有信息,请确定是否导入?')">导入</a> 
    | <a href="?c=Data&a=del&ids[]=<?php echo $this->data['val']['filename'];?>" onClick="return confirm('确定删除?')">删除</a> 
    </td>
</tr>
<?php }
?>
<tr class="tr">
<td colspan="6">说明:导入时只需要点导入任意一个文件,程序会自动导入剩余文件!</td>
</tr>
<tr>
<td colspan="6" class="tr"><input type="button" class="bginput" id="checkall" value="全/反选" onClick="$.selectAll('ids[]');"> <input type="submit" class="bginput" value="批量删除" onClick="return confirm('确定删除?')"/></td>
</tr>

</table>
</form>
<?php $this->display('footer.html');?>