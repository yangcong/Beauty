<?php
 /* compiled by (WeePHP) at (2014-08-14 14:01:49) */

 $this->display('header.html');
 if($this->data['list']){?>
<table width="98%" border="0" cellpadding="4" cellspacing="1" class="table">
  <tr class="table_title">
    <td colspan="5">广告列表</td>
  </tr>
  <tr class="list_head ct">
    <td width="60">编号</td>
    <td >广告标识</td>
    <td width="250">说明</td>
    <td>广告代码</td>
    <td width="150">操作</td>
  </tr>
  <tbody class="list_tbody">
    <?php foreach($this->data['list'] as $this->data['val']){?>
    <tr class="tr ct">
      <td class="td"><?php echo $this->data['val']['id'];?></td>
      <td class="td"><?php echo $this->data['val']['title'];?> </td>
      <td class="td"><?php echo $this->data['val']['des'];?></td>
      <td class="td"><textarea rows="3" disabled="disabled" style="width:98%"><?php echo htmlspecialchars($this->data['val']['content']);?></textarea></td>
      <td class="td"><a href="?c=Adsense&a=preview&id=<?php echo $this->data['val']['id'];?>" target="_blank">预览</a> | <a href="?c=Adsense&id=<?php echo $this->data['val']['id'];?>#add_edit_link">修改</a> | <a href="?c=Adsense&id=<?php echo $this->data['val']['id'];?>&a=del" onclick="return confirm('确定删除吗?')">删除</a></td>
    </tr>
    <?php }
?>
  </tbody>
</table>
<?php }
?>

<form action="?c=Adsense&a=add" method="post">
  <table width="98%" border="0" cellpadding="4" cellspacing="1" class="table">
    <tr class="table_title" id="add_edit_link">
      <td colspan="2"> <?php if($this->data['id']){?>修改<?php } else{?>添加<?php }
?>广告</td>
    </tr>
    <tr class="tr">
      <td width="100" >广告标识:</td>
      <td ><input name="title" type="text" maxlength="50" value="<?php echo $this->data['title'];?>" style="width:200px">
        * </td>
    </tr>
    <tr class="tr">
      <td width="100" >说明:</td>
      <td ><input name="des" type="text" maxlength="50" value="<?php echo $this->data['des'];?>" style="width:200px">
       </td>
    </tr>
    <tr class="tr">
      <td >广告代码:</td>
      <td ><textarea style="height:100px; width:400px" cols="80" name="content"><?php echo $this->data['content'];?></textarea>
        *</td>
    </tr>
    <tr class="tr">
      <td>&nbsp;</td>
      <td><input type="hidden" name="id" value="<?php echo $this->data['id'];?>" />
      <input class="bginput" type="submit" name="submit" value="提交" /></td>
    </tr>
  </table>
</form>
<?php $this->display('footer.html');?>