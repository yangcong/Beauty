<?php
 /* compiled by (WeePHP) at (2014-08-14 15:29:07) */

 $this->display('header.html');?>
<form action="?c=Config&type=url"  method="post" id="myform">
  <table width="98%" border="0" cellpadding="4" cellspacing="1" class="table">
    <tr class="table_title">
      <td colspan="4">访问路径设置</td>
    </tr>
    <tr class="ji">
      <td class="left">全局网站访问模式</td>
      <td colspan="3"><select name="con[url_mode]" id="url_mode">
          <option value="0" selected="selected" <?php if('0' == $this->data['url_mode']){?>selected<?php }
?>>动态1：[?c=Cate&cid=1&p=1] </option>
          <option value="1" <?php if('1' == $this->data['url_mode']){?>selected<?php }
?>>动态2：[?Cate-cid-1.html]</option>
          <option value="2" <?php if('2' == $this->data['url_mode']){?>selected<?php }
?>>伪静态：[/Cate-cid-1.html]</option>
        </select>
        <a href="http://www.piccms.com/thread-143-1-1.html" target="_blank"><img src="images/admin/faq.gif" /></a> 使用伪静态需要服务器支持Rewrite重写 </td>
    </tr>
    <tr class="ji">
      <td class="left">伪静态和HTML后缀名</td>
      <td colspan="3"><select name="con[url_suffix]">
          <option value=".html" <?php if('.html' == $this->data['url_suffix']){?>selected<?php }
?>>.html</option>
          <option value=".htm" <?php if('.htm' == $this->data['url_suffix']){?>selected<?php }
?>>.htm</option>
          <option value=".shtml" <?php if('.shtml' == $this->data['url_suffix']){?>selected<?php }
?>>.shtml</option>
          <option value=".shtm" <?php if('.shtm' == $this->data['url_suffix']){?>selected<?php }
?>>.shtm</option>
        </select></td>
    </tr>
    <tr class="ji"  >
      <td class="left">首页访问模式</td>
      <td colspan="3"><select name="con[url_html_index]" id="url_html_index">
          <option value="1" <?php if('1' == $this->data['url_html_index']){?>selected<?php }
?>>静态生成</option>
          <option value="0" <?php if('0' == $this->data['url_html_index']){?>selected<?php }
?>>动态</option>
        </select></td>
    </tr>
    <tr class="ji"  >
      <td class="left">分类页访问模式</td>
      <td colspan="3"><select name="con[url_html_cate]" id="url_html_cate" onchange="if(1==this.value){$('#url_dir_cate').show()}else{$('#url_dir_cate').hide()}">
          <option value="1" <?php if('1' == $this->data['url_html_cate']){?>selected<?php }
?>>静态生成</option>
          <option value="0" <?php if('0' == $this->data['url_html_cate']){?>selected<?php }
?>>动态</option>
        </select>
        <span id="url_dir_cate" <?php if('0' == $this->data['url_html_cate']){?>style="display:none"<?php }
?>>分类页保存目录：
        <input name="con[url_dir_cate]" type="text"  id="con[url_dir_cate]" value="<?php echo $this->data['url_dir_cate'];?>" maxlength="30">
        </span></td>
    </tr>
    <tr class="ji"  >
      <td class="left">内容页访问模式</td>
      <td colspan="3"><select name="con[url_html_content]" id="url_html_content"  onchange="if(1==this.value){$('#url_dir_content').show()}else{$('#url_dir_content').hide()}">
          <option value="1" <?php if('1' == $this->data['url_html_content']){?>selected<?php }
?>>静态生成</option>
          <option value="0" <?php if('0' == $this->data['url_html_content']){?>selected<?php }
?>>动态</option>
        </select>
        <span id="url_dir_content" <?php if('0' == $this->data['url_html_content']){?>style="display:none"<?php }
?>> 内容页保存目录：
        <input name="con[url_dir_content]" type="text" id="con[url_dir_content]" value="<?php echo $this->data['url_dir_content'];?>" maxlength="30">
        </span></td>
    </tr>
    <tr class="ji"  >
      <td class="left">RSS网站地图访问模式</td>
      <td colspan="3"><select name="con[url_html_maps]" id="url_html_maps"  onchange="if(1==this.value){$('#url_dir_maps').show()}else{$('#url_dir_maps').hide()}">
          <option value="1" <?php if('1' == $this->data['url_html_maps']){?>selected<?php }
?>>静态生成</option>
          <option value="0" <?php if('0' == $this->data['url_html_maps']){?>selected<?php }
?>>动态</option>
        </select>
        <span id="url_dir_maps" <?php if('0' == $this->data['url_html_maps']){?>style="display:none"<?php }
?>> 网站地图保存目录：
        <input name="con[url_dir_maps]" type="text"  id="con[url_dir_maps]" value="<?php echo $this->data['url_dir_maps'];?>" maxlength="30">
        </span></td>
    </tr>
    <tr class="tr" >
      <td class="left">网页生成时间间隔</td>
      <td colspan="3"><input type="text" name="con[url_create_time]" maxlength="6" value="<?php echo $this->data['url_create_time'];?>" >
        静态生成内容页过程中暂停几秒 </td>
    </tr>
    <tr class="ji" >
      <td class="left">每页生成文件数量</td>
      <td colspan="3"><input type="text" name="con[url_create_num]" maxlength="6" value="<?php echo $this->data['url_create_num'];?>" >
        静态生成网页时每一页生成的数量 </td>
    </tr>
    <tr class="tr">
      <td colspan="4"><input type="hidden" name="setting_sub" value="true">
        <input class="bginput" type="submit" name="submit" value="提交">
        <input class="bginput" type="reset" name="Input" value="重置" ></td>
    </tr>
  </table>
</form>
<?php $this->display('footer.html');?>