<?php
 /* compiled by (WeePHP) at (2014-08-14 13:57:04) */

 $this->display('header.html');?>

<script type="text/javascript">
function setStauts(cid, status) {
	$.get('?c=Cate&a=setStatus&cid=' + cid + '&status=' + status, null, function(rs){self.location='?c=Cate&a=show'});
}
</script>
<form action="?c=Cate&a=show" method="post" name="myform" id="myform">
<table width="98%" border="0" cellpadding="4" cellspacing="1" class="table">
  <tr class="table_title">
    <td colspan="14">栏目分类列表</td>
  </tr>
  <tr class="list_head ct">
    <td width="50">编号</td>
    <td>栏目名称</td>
    <td>别名</td>
    <td>模板</td>
    <td>显示模式</td>
    <td width="250">操作</td>
    <td width=70>排序</td>
  </tr>
  
  <tbody class="list_tbody">
  <?php foreach($this->data['cTree'] as $this->data['value']){?>
  <tr class="tr ct">
    <td class="lt"><input type='checkbox' name='ids[]' value='<?php echo $this->data['value']['cid'];?>' class="noborder"><?php echo $this->data['value']['cid'];?></td>
    <td class="lt">【<a href="<?php echo $this->data['value']['url'];?>" target="_blank"><?php echo $this->data['value']['name'];?></a>】</td>
    
    <td class="td"><neq name="gxcms.mid" value="9"><?php echo $this->data['value']['eng_name'];?></neq></td>
    <td class="td"><?php if($this->data['value']['ctpl']){
 echo $this->data['value']['ctpl'];
 } else{?>默认<?php }
?></td>
    <td class="td"><?php if(1==$this->data['value']['view_type']){?>幻灯模式<?php } else{?>普通模式<?php }
?></td>
    <td class="td">
      <a href="?c=Article&a=add&cid=<?php echo $this->data['value']['cid'];?>">添加内容</a> | 
      <a href="?c=Article&a=show&cid=<?php echo $this->data['value']['cid'];?>">查看内容</a> | 
      <?php if($this->data['value']['status']){?>
      <a href="javascript:void(0)" onclick="setStauts(<?php echo $this->data['value']['cid'];?>, 0);" title="点击切换成导航栏上隐藏">显示</a>
      <?php } else{?>
      <a class="red" href="javascript:void(0)" onclick="setStauts(<?php echo $this->data['value']['cid'];?>, 1);" title="点击切换成导航栏上显示">隐藏</a>
      <?php }
?>
      |    
      <a href="?c=Cate&a=add&cid=<?php echo $this->data['value']['cid'];?>">编辑</a> | 
      <a href="?c=Cate&a=delete&ids=<?php echo $this->data['value']['cid'];?>" onclick="return confirm('确定删除该分类吗?删除后将不能恢复！')">删除</a>
      </td>
    <td class="td"><input type='text' name='oid[<?php echo $this->data['value']['cid'];?>]' value='<?php echo $this->data['value']['oid'];?>' style="width:22px;" maxlength="3"></td>
  </tr>
  <?php foreach($this->data['value']['son'] as $this->data['val']){?> 
  <tr class="tr ct">
    <td class="lt"><input type='checkbox' name='ids[]' value='<?php echo $this->data['val']['cid'];?>' class="noborder"><?php echo $this->data['val']['cid'];?></td>
    <td class="lt">&nbsp;&nbsp;├【<a href="<?php echo $this->data['val']['url'];?>" target="_blank"><?php echo $this->data['val']['name'];?></a>】</td>

    <td class="td"><neq name="gxcms.mid" value="9"><?php echo $this->data['val']['eng_name'];?></neq></td>
    <td class="td"><?php if($this->data['val']['ctpl']){
 echo $this->data['val']['ctpl'];
 } else{?>默认<?php }
?></td>
    <td class="td"><?php if(1==$this->data['val']['view_type']){?>幻灯模式<?php } else{?>普通模式<?php }
?></td>
    <td class="td"><neq name="gxcms.mid" value="9">
      <a href="?c=Article&a=add&cid=<?php echo $this->data['val']['cid'];?>">发表内容</a> | 
      <a href="?c=Article&a=show&cid=<?php echo $this->data['val']['cid'];?>">查看内容</a> | 
      <?php if($this->data['val']['status']){?>
      <a href="javascript:void(0)" onclick="setStauts(<?php echo $this->data['val']['cid'];?>, 0);" title="点击切换成导航栏上隐藏">显示</a>
      <?php } else{?>
      <a class="red" href="javascript:void(0)" onclick="setStauts(<?php echo $this->data['val']['cid'];?>, 1);" title="点击切换成导航栏上显示">隐藏</a>
      <?php }
?>    
      | 
      <a href="?c=Cate&a=add&cid=<?php echo $this->data['val']['cid'];?>">编辑</a> | 
      <a href="?c=Cate&amp;a=delete&amp;ids=<?php echo $this->data['val']['cid'];?>" onclick="return confirm('确定删除该分类吗?删除后将不能恢复！')">删除</a></td>
    <td class="td"><input type='text' name='oid[<?php echo $this->data['val']['cid'];?>]' value='<?php echo $this->data['val']['oid'];?>' style="width:22px;" maxlength="3"></td>
  </tr>
  <?php }

 }
?>
</tbody>
  <tr>
    <td colspan="14" bgcolor="#FFFFFF">
    <input type="hidden" name="submit" value="true" />
    <input onclick="$.selectAll('ids[]');" class="bginput" type="button" id="checkall" value="全/反选">
    <input type="submit" value="批量删除" onclick="if(confirm('删除后将无法还原,确定要删除吗?')){myform.action='?c=Cate&a=delete';}else{return false}" class="bginput"/>
    <input type="submit" value="修改排序" class="bginput"  onclick="myform.action='?c=Cate&a=updateOid';"/> 
       &nbsp;注删除分类后原分类下的文章将被全部删除</td>
  </tr>
</table>
</form>
<?php $this->display('footer.html');?>