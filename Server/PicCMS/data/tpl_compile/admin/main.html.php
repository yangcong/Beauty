<?php
 /* compiled by (WeePHP) at (2014-08-14 13:55:56) */

 $this->display('header.html');?>
<table width="98%" border="0" cellpadding="4" cellspacing="1" class="table">
  <tr class="table_title">
    <td colspan="2">系统环境检测</td>
  </tr>
  <tr class="ji">
    <td width="200" class="rt">主机名 (IP端口)</td>
    <td ><?php 
echo $this->data['_SERVER']['SERVER_NAME'].' ('.$this->data['_SERVER']['SERVER_ADDR'].':'.$this->data['_SERVER']['SERVER_PORT'].')'
?></td>
  </tr>
  <tr class="ou">
    <td class="rt">程序目录</td>
    <td><?php echo $this->data['app_path'];?></td>
  </tr>
  <tr class="ou">
    <td class="rt">安装路径</td>
    <td><?php echo $this->data['web_dir'];?></td>
  </tr>
  <tr class="ji">
    <td class="rt">Web服务器</td>
    <td><?php 
echo $this->data['_SERVER']['SERVER_SOFTWARE']
?></td>
  </tr>
  <tr class="ji">
    <td class="rt">服务器时间</td>
    <td><?php 
echo gmdate("Y-m-d H:i:s", time()+8*60*60)
?> <span class="blue">(+08:00)</span></td>
  </tr>
  <tr class="ou">
    <td class="rt">PHP 运行方式</td>
    <td><?php 
echo PHP_SAPI
?></td>
  </tr>
  <tr class="ji">
    <td class="rt">PHP版本</td>
    <td><?php 
echo PHP_VERSION
?></td>
  </tr>
  <tr class="ou">
    <td class="rt">MySQL 版本</td>
    <td><?php 
if (function_exists("mysql_close")) {
    		echo mysql_get_client_info();
        }else{
        	echo '不支持';
        }
?>&nbsp;&nbsp;</td>
  </tr>
  <tr class="ji">
    <td class="rt">GD库版本</td>
    <td><?php 

    	if(function_exists('gd_info')){
    	 	$this->data['gd'] = gd_info();
            echo $this->data['gd']['GD Version'];
    	}else{
        	echo "不支持";
        }
        
?></td>
  </tr>
   <tr class="ji">
    <td class="rt">Zend Optimizer</td>
    <td><?php 

    	if(defined("OPTIMIZER_VERSION")){
    		echo OPTIMIZER_VERSION;
    	} else {
        	echo "不支持";
        }
        
?></td>
  </tr>
  <tr class="ou">
    <td class="rt">最大上传限制</td>
    <td><?php 
if (ini_get('file_uploads')) {
    		echo ini_get('upload_max_filesize');
        }else{
        	echo '<span style="color:red">Disabled</span>';
    }
?></td>
  </tr>
  <tr class="ji">
    <td class="rt">最大执行时间</td>
    <td><?php 
echo ini_get('max_execution_time')
?>秒</td>
  </tr>
  <tr class="ou">
    <td class="rt">采集函数检测</td>
    <td><?php 
if (ini_get('allow_url_fopen')) {
    		echo '支持';
        }else{
        	echo '<span style="color:red">不支持</span>';
    }
?></td>
  </tr> 
  <tr class="ou">
    <td class="rt">FTP函数检测</td>
    <td><?php 
if (function_exists('ftp_connect')) {
    		echo '支持';
        }else{
        	echo '<span style="color:red">不支持</span>';
    }
?></td>
  </tr>
     
</table>
<table width="98%" border="0" cellpadding="4" cellspacing="1" class="table">
  <tr class="table_title">
    <td colspan="2">产品说明</td>
  </tr>
  <tr class="ji">
    <td width="200" class="rt">官方主页</td>
    <td ><a href="<?php echo $this->data['sys_url'];?>" target="_blank"><?php echo $this->data['sys_url'];?></a></td>
  </tr>
  <tr class="ji">
    <td width="200" class="rt">开发团队</td>
    <td >小鱼哥哥(Email: 29500196@qq.com) </td>
  </tr>
  <tr class="ji">
    <td width="200" class="rt">最新版本</td>
    <td >v1.0</td>
  </tr>
</table>
<?php $this->display('footer.html');?>