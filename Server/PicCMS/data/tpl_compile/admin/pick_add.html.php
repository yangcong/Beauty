<?php
 /* compiled by (WeePHP) at (2014-08-14 13:56:51) */

 $this->display('header.html');?>
<script type="text/javascript">
// 测试序列地址
function testListUrl() {
	var data = $('#myform').serializeArray();
	$.fopen('?c=Pick&a=testListUrl', data, '测试列表地址');
}

// testListRule
function testListRule() {
	var data = $('#myform').serializeArray();
	$.fopen('?c=Pick&a=testListRule', data, '测试列表规则');
}

// testContentRule
function testContentRule() {
	var data = $('#myform').serializeArray();
	$.fopen('?c=Pick&a=testContentRule', data, '测试内容规则');
}

// testShowPage
function testShowPage() {
	var data = $('#myform').serializeArray();
	$.fopen('?c=Pick&a=testShowPage', data, '测试分页规则');	
}

</script>


<form action="?c=Pick&a=add" method="post" id="myform" name="add">
  <table width="98%" border="0" cellpadding="4" cellspacing="1" class="table">
    <tr class="tabs_title">
      <td colspan="2">
      <span id="tabs" class="fl"> 
      <a class="tab2 on" id="tab_1" href="javascript:void(0)" onclick="$.showTab(1, 3)" >列表规则</a> 
      <a class="tab2" id="tab_2" href="javascript:void(0)" onclick="$.showTab(2, 3)" >内容规则</a> 
      <a class="tab2" id="tab_3" href="javascript:void(0)" onclick="$.showTab(3, 3)" >分页规则</a> 
      </span> 
      <span class="fr"><a href="?c=Pick&amp;a=show" class="no">返回采集管理</a></span></td>
    </tr>
  </table>
  <div>
    <table width="98%" border="0" cellpadding="4" cellspacing="1" class="table" id="showtab_1" style="margin:0 5px;">
      <tbody>
        <tr class="ji">
          <td width="150" class="rt" >规则名称</td>
          <td><input type="text" name="webname" class="input" value="<?php echo $this->data['webname'];?>" /></td>
        </tr>
        <tr class="ji">
          <td width="100" class="rt" >分类</td>
          <td>
          <select name="status" class="select">
              <option value="0">文章状态</option>
              <option value="1" <?php if('1'==$this->data['status']){?>selected<?php }
?>>显示</option>
              <option value="0" <?php if('0'==$this->data['status']){?>selected<?php }
?>>隐藏</option>
          </select>
          <?php echo $this->data['cTreeStr'];?> 要导入的分类
            
          </td>
        </tr>
        <tr class="ji">
          <td class="rt" >目标网站编码</td>
          <td>
          <label><input type="radio" name="charset_type" value="GBK" <?php if('GBK'==$this->data['charset_type']){?>checked="checked"<?php }
?> />
	GBK</label>
 <label> <input type="radio" name="charset_type" value="UTF-8" <?php if('UTF-8'==$this->data['charset_type']){?>checked="checked"<?php }
?>/> 
  UTF-8</label>
  <label><input type="radio" name="charset_type" value="BIG5" <?php if('BIG5'==$this->data['charset_type']){?>checked="checked"<?php }
?>/>
  BIG5</label>&nbsp;&nbsp;&nbsp;<span class="red">必须正确设置目标网站的编码</span></td>
        </tr>
        <tr class="ji">
          <td class="rt">序列地址</td>
          <td>
            <input type="text" name="listurl" id="listurl" class="input" value="<?php echo $this->data['listurl'];?>"/> 		
            <a class="button" href="javascript:void(0)" onclick="testListUrl();">测试</a>
            <p>
              如http://www.xxx.com/lists/p/[page].html, 页码使用[page]做为通配符
            </p>
            <p>

            页码从:
            <input name="page_begin" id="page_begin" type="text" value="<?php echo $this->data['page_begin'];?>" size="5" />
            到
            <input name="page_end" id="page_end" type="text" value="<?php echo $this->data['page_end'];?>" size="5" />
            每次递增
            <input name="page_step" id="page_step" type="text" value="<?php echo $this->data['page_step'];?>" size="5" />
           </p>
          
          </td>
        </tr>
        <tr class="ji" id"cover">
          <td class="rt">额外不规则地址</td>
          <td><span style="display:<?php if(0 == $this->data['rulepage']){?>block<?php } else{?>none<?php }
?>">
            <textarea name="listmoreurl" cols="70" rows="7"><?php echo $this->data['listmoreurl'];?></textarea>
          </span></td>
        </tr>
        <tr class="ji">
          <td width="150" class="rt">列表内容区域规则</td>
          <td><p>
            <textarea rows="7" cols="70" name="list_content_rule" class="input_textarea" id="list_content_rule"><?php echo $this->data['list_content_rule'];?></textarea>
          </p>
          <p>如: &lt;div class='page'&gt;<span class="blue">[content]</span>&lt;/div&gt;, 通配符 [content]: 分页区域, [*]: 任意字符</p></td>
        </tr>
        <tr class="ji">
          <td class="rt">列表页地址和标题规则</td>
          <td>
            <textarea name="title_rule" cols="70" rows="7"><?php echo $this->data['title_rule'];?></textarea>
          <p>如: &lt;a href=&quot;<span class="blue">[url]</span>&quot; title=&quot;<span class="blue">[title]</span>&quot;&gt;&lt;img src=<span class="blue">[*]</span>&gt;&lt;br&gt;<span class="blue">[*]</span>&lt;/a&gt; 通配符 [url]: 地址, [title]: 标题, [*]: 任意字符</p></td>
        </tr>
        <tr class="tr">
          <td class="rt">地址过滤规则</td>
          <td><span class="rt">地址</span>中必须包含
            <input name="link_include_word" type="text" value="<?php echo $this->data['link_include_word'];?>" />
            不得包含
<input name="link_noinclude_word" type="text" value="<?php echo $this->data['link_noinclude_word'];?>" />
<a class="button" href="javascript:void(0)" onclick="testListRule();">测试</a></td>
        </tr>
        <tr class="ji">
          
        </tr>
      </tbody>
    </table>
  </div>
  
  <div>
    <table width="98%" border="0" cellpadding="4" cellspacing="1" class="table" id="showtab_2" style="margin:0 5px; display:none">
      <tbody>
        <tr class="ji">
          <td width="150" class="rt">图片内容区域规则</td>
          <td><p>
            <textarea rows="7" cols="70" name="content_rule" class="input_textarea" id="content_rule"><?php echo $this->data['content_rule'];?></textarea>
          </p>
          <p>如: &lt;div class='contert'&gt;<span class="blue">[content]</span>&lt;/div&gt;, 通配符 [content]: 内容区域, [*]: 任意字符</p></td>
        </tr>
        
        <tr class="ji">
          <td width="100" class="rt">图片地址规则</td>
          <td><p>
            <textarea name="file_rule" cols="70" rows="7"><?php echo $this->data['file_rule'];?></textarea>
          </p>
          <p>如: &lt;img src=&quot;<span class="blue">[url]</span>&quot; alt=&quot;<span class="blue">[title]</span>&quot; /&gt;, 通配符 [url]: 图片地址, [title]: 图片标题, [*]: 任意字符</p></td>
        </tr>
        
        <tr class="ji">
          <td width="100" class="rt">地址过滤规则</td>
          <td><span class="rt">地址</span>中必须包含
            <input name="file_include_word" type="text" id="file_include_word" value="<?php echo $this->data['file_include_word'];?>" />
不得包含
<input name="file_noinclude_word" type="text" id="file_noinclude_word" value="<?php echo $this->data['file_noinclude_word'];?>" /> <a class="button" href="javascript:void(0)" onclick="testContentRule();">测试</a></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div>
    <table width="98%" border="0" cellpadding="4" cellspacing="1" class="table" id="showtab_3" style=" margin:0 5px; display:none">
      <tbody>
        <tr class="ji">
          <td width="150" class="rt">分页内容区域规则</td>
          <td><p>
            <textarea rows="7" cols="70" name="page_content_rule" class="input_textarea" id="page_content_rule"><?php echo $this->data['page_content_rule'];?></textarea>
          </p>
          <p>如: &lt;div class='page'&gt;<span class="blue">[content]</span>&lt;/div&gt;, 通配符 [content]: 分页区域, [*]: 任意字符</p></td>
        </tr>
        
        <tr class="ji">
          <td width="100" class="rt">分页地址规则</td>
          <td><p>
            <textarea name="page_rule" cols="70" rows="7" id="page_rule"><?php echo $this->data['page_rule'];?></textarea>
          </p>
          <p>如: &lt;a href=&quot;<span class="blue">[url]</span>&quot;&gt;<span class="blue">[*]</span>&lt;/a&gt;, 通配符 [url]: 分页地址, [*]: 任意字符   <a class="button" href="javascript:void(0)" onclick="testShowPage();">测试</a></p></td>
        </tr>
      </tbody>
    </table>
  </div>
  <table width="98%" border="0" cellpadding="4" cellspacing="1" class="table">
    <tr class="tr ct">
      <td align="left" style="padding-left:150px;">
      <input type="hidden" name="id" value="<?php echo $this->data['id'];?>" />
      <input class="bginput" type="submit" value="提交" name="submit" />
      <input class="bginput" type="reset" name="Input" value="重置" /></td>
    </tr>
  </table>
</form>
<?php $this->display('footer.html');?>