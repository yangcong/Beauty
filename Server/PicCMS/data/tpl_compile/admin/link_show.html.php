<?php
 /* compiled by (WeePHP) at (2014-08-14 14:01:48) */

 $this->display('header.html');
 if($this->data['linkList']){?>
<table width="98%" border="0" cellpadding="4" cellspacing="1" class="table">
  <tr class="table_title">
    <td colspan="7">友情链接列表</td>
  </tr>
  <tr class="list_head ct">
    <td width="60">编号</td>
    <td >网站名称</td>
    <td width="250">网站地址</td>
    <td width="250">网站LOGO</td>
    <td width="70">顺序</td>
    <td width="70">形式</td>
    <td width="100">操作</td>
  </tr>
  <tbody class="list_tbody">
    <?php foreach($this->data['linkList'] as $this->data['val']){?>
    <tr class="tr ct">
      <td class="td"><?php echo $this->data['val']['id'];?></td>
      <td class="td"><?php echo $this->data['val']['title'];?> </td>
      <td class="td"><?php echo $this->data['val']['url'];?></td>
      <td class="td"><?php if($this->data['val']['logo']){?><img src="<?php echo $this->data['val']['logo'];?>" /><?php }
?></td>
      <td class="td"><?php echo $this->data['val']['oid'];?></td>
      <td class="td"> <?php if("1"==$this->data['val']['type']){?>
        文字
        <?php } else{?>
        图片
        <?php }
?> </td>
      <td class="td"><a href="?c=Link&id=<?php echo $this->data['val']['id'];?>#add_edit_link">修改</a> | <a href="?c=Link&id=<?php echo $this->data['val']['id'];?>&a=del" onclick="return confirm('确定删除该友情链接吗?')">删除</a></td>
    </tr>
    <?php }
?>
  </tbody>
</table>
<?php }
?>

<form action="?c=Link&a=add" method="post">
  <table width="98%" border="0" cellpadding="4" cellspacing="1" class="table">
    <tr class="table_title" id="add_edit_link">
      <td colspan="2"> <?php if($this->data['id']){?>修改<?php } else{?>添加<?php }
?>友情链接</td>
    </tr>
    <tr class="tr">
      <td width="100" >网站名称:</td>
      <td ><input name="title" type="text" maxlength="50" value="<?php echo $this->data['title'];?>" style="width:200px">
        *</td>
    </tr>
    <tr class="tr">
      <td >网站地址:</td>
      <td ><input name="url" type="text" size="40" value="<?php echo $this->data['url'];?>" maxlength="250" style="width:200px">
        *</td>
    </tr>
    <tr class="tr">
      <td >LOGO地址：</td>
      <td ><input name="logo" type="text" size="40" value="<?php echo $this->data['logo'];?>" maxlength="250" style="width:200px"></td>
    </tr>
    <tr class="tr">
      <td >连接排序：</td>
      <td ><input name="oid" type="text" value="<?php echo $this->data['oid'];?>" maxlength="5" style="width:40px;text-align:center" title="越小越前面">
        越小越前面</td>
    </tr>
    <tr class="tr">
      <td >链接类型：</td>
      <td ><select name="type">
          <option value="1" <?php if("1" == $this->data['type']){?>selected<?php }
?>>文字</option>
          <option value="2" <?php if("2" == $this->data['type']){?>selected<?php }
?>>图片</option>
        </select></td>
    </tr>
    <tr class="tr">
      <td>&nbsp;</td>
      <td><input type="hidden" name="id" value="<?php echo $this->data['id'];?>" />
      <input class="bginput" type="submit" name="submit" value="提交" /></td>
    </tr>
  </table>
</form>
<?php $this->display('footer.html');?>