<?php
 /* compiled by (WeePHP) at (2014-08-14 14:01:52) */

 $this->display('header.html');?>

<form action="?s=Admin/Html/Maps" method="post" id="myform" name="myform">
<table width="98%" border="0" cellpadding="4" cellspacing="1" class="table">
    <tr class="table_title">
      <td colspan="2">静态网页生成选项</td>
    </tr>
    <tr class="tr">
      <td width="200" class="rt" >一键生成</td>
      <td ><input type="submit" value="一键生成全站" class="bginput" onclick="myform.action='?c=Html&a=makeIndex&oneKey=true';" /></td>
    </tr>
    <tr class="tr">
      <td class="rt">生成网站首页</td>
      <td ><input type="submit" value="开始生成" class="bginput" onclick="myform.action='?c=Html&a=makeIndex';" <?php if(!$this->data['url_html_index']){?>disabled<?php }
?>/></td>
    </tr>
    <tr class="tr">
      <td  class="rt">生成分类列表
        </td>
      <td > <?php echo $this->data['cateStr'];?>
      <input type="submit" value="开始生成" class="bginput" onclick="myform.action='?c=Html&a=makeCate';" <?php if(!$this->data['url_html_cate']){?>disabled<?php }
?> /></td>
    </tr>
    <tr class="tr">
      <td  class="rt">生成文章内容        </td>
      <td width="836" ><?php echo $this->data['conCateStr'];?>
      <input type="submit" value="开始生成" class="bginput" onclick="myform.action='?c=Html&a=makeArticle';" <?php if(!$this->data['url_html_content']){?>disabled<?php }
?>/></td>
    </tr>
    <tr class="tr">
      <td  class="rt">文章按时间生成</td>
      <td width="836" ><select name="mday">
          <option value="1">当天</option>
          <option value="2">2天内</option>
          <option value="3">3天内</option>
          <option value="4">4天内</option>
          <option value="5">5天内</option>
          <option value="6">6天内</option>
          <option value="7">一周内</option>
          <option value="31">一月内</option>
        </select>
        <input type="submit" value="开始生成" class="bginput" onclick="myform.action='?c=Html&a=makeByAddtime';" <?php if(!$this->data['url_html_content']){?>disabled<?php }
?>/>
      </td>
    </tr>
    <tr class="tr">
      <td  class="rt">生成RSS网站地图
        </td>
      <td ><input type="submit" value="开始生成" class="bginput" onclick="myform.action='?c=Html&a=makeMaps';" <?php if(!$this->data['url_html_maps']){?>disabled<?php }
?>/>
      sitempa.xml 和 rss.xml</td>
    </tr>
</table>
</form>
<?php $this->display('footer.html');?>