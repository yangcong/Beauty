<?php
 /* compiled by (WeePHP) at (2014-08-14 14:01:50) */

 $this->display('header.html');?>

<script type="text/javascript">
/*排序*/
function orderby(order, by) {
	$('#order').val(order);
	$('#by').val(by);
	search_submit();
}
/*分页*/
function showpage(p) {
	$('#p').val(p);
	search_submit();
}
/*状态*/
function show_type(type) {
	$('#type').val(type);
	search_submit();
}
function search_submit() {
	$('#myform').attr('action', '?c=Comment');
	$('#myform').submit();	
}

/*设置状态*/
function setStatus(id, status) {
	if (!id || id.length == 0) {
		return $.alerts.alert("请选择一张图片");	
	}
	$.post('?c=Comment&a=setStatus',
		{id: id, status: status},
		function(rs) {
			search_submit();
		}
	);
}


/*删除*/
function delit(id){
	if (!id || id.length == 0) {
		return $.alerts.alert("请选择一条评论");	
	}
	$.post("?c=Comment&a=del", 
		{id: id}, 
		function(data){
			search_submit();
		}
	);
}
</script>
  <form action="?c=Comment" method="post" name="myform" id="myform">
  <table width="98%" border="0" cellpadding="5" cellspacing="1" class="table">

    <tr>
      <td colspan="8" class="table_title"><span class="fl">评论管理</span></td>
    </tr>
      <tr class="tr">
      <td height="36" colspan="9">
      
        <input name="order" type="hidden" id="order" value="<?php echo $this->data['order'];?>">
        <input name="by" type="hidden" id="by" value="<?php echo $this->data['by'];?>">
        <input name="p" type="hidden" id="p" value="<?php echo $this->data['p'];?>">
        <input name="article_id" type="hidden" id="article_id" value="<?php echo $this->data['article_id'];?>" />
        状态
        <select name="status" class="select">
          <option value="ALL" <?php if('all'==$this->data['status']){?>selected<?php }
?>>全部</option>
          <option value="1" <?php if('1'==$this->data['status']){?>selected<?php }
?>>显示</option>
          <option value="0" <?php if('0'==$this->data['status']){?>selected<?php }
?>>隐藏</option>
        </select>
        内容 
        <input type="text" value="<?php echo $this->data['keyword'];?>" size="20" id="keyword" name="keyword">
        <input type="submit" onclick="search_submit();" value="搜 索" class="bginput" />
        <input type="button" onclick="self.location='?c=Comment'" value="显示所有" class="bginput" /></td>
    </tr> 
    
    <tr class="list_head ct">
      <td width="80">编号id
        <?php if('DESC'==$this->data['by']){?>
        <a href="javascript:void(0)" onclick="orderby('id', 'ASC');"><img src="images/admin/up.gif" border="0" title="点击按ID升序排列"></a>
      	<?php } else{?>
      	<a href="javascript:void(0)" onclick="orderby('id', 'DESC');"><img src="images/admin/down.gif" border="0" title="点击按ID降序排列"></a>
      	<?php }
?>
      </td>
      <td >所属文章</td>
      <td>内容</td>
      <td width="70">作者
      <td width="70">IP            
      <td width="150">      更新时间
        <?php if('DESC'==$this->data['by']){?>
        <a href="javascript:void(0)" onclick="orderby('dateline', 'ASC');"><img src="images/admin/up.gif" border="0" title="点击按时间升序排列"></a>
         <?php } else{?>
        <a href="javascript:void(0)" onclick="orderby('dateline', 'DESC');"><img src="images/admin/down.gif" border="0" title="点击按时间降序排列"></a><?php }
?>
      
      <td width="100" >操作</td>
    </tr>
    
    <tbody class="list_tbody">
    <?php foreach($this->data['list'] as $this->data['val']){?>
      <tr class="tr">
        <td ><input name='ids[]' type='checkbox' value='<?php echo $this->data['val']['id'];?>' class="noborder">
          <?php echo $this->data['val']['id'];?></td>
        <td class='lt'><a href="?c=Comment&article_id=<?php echo $this->data['val']['article_id'];?>"><?php echo $this->data['val']['title'];?></a></td>
        <td class="lt"><?php echo $this->data['val']['content'];?></td>
        <td class="ct"><?php echo $this->data['val']['user_name'];?>
       </td>
        <td class="ct"><?php echo $this->data['val']['ip'];?></td>
        <td class="td ct"><?php echo Ext_Date::format($this->data['val']['dateline']);?></td>
        <td class="td ct"> 
        <a href="javascript:void(0)" onclick="$.alerts.confirm('确认要删除? 删除后不可恢复!', null, function(r){ if(r){ delit(<?php echo $this->data['val']['id'];?>) } })" title="点击删除附件">删除</a> | <?php if($this->data['val']['status']){?>
       	<a href="javascript:void(0)" onclick="setStatus(<?php echo $this->data['val']['id'];?>, 0);" title="点击隐藏附件">显示</a> <?php } else{?> <a class="red" href="javascript:void(0)" onclick="setStatus(<?php echo $this->data['val']['id'];?>, 1);" title="点击显示附件">隐藏</a> <?php }
?></td>
      </tr>
    <?php }
?>
    </tbody>
    
    <tr class="tr">
      <td colspan="9"><div class="pagelist"><?php echo $this->data['pageHtml'];?></div></td>
    </tr>  
</table>
<table width="98%" border="0" cellpadding="5" cellspacing="1" class="table">
	<tr class="tr">
      <td colspan="9" valign="middle"><input type="button" onclick="$.selectAll('ids[]');" id="checkall" value="全/反选" class="bginput">
        &nbsp;&nbsp;
        <input type="button" value="批量审核" class="bginput" onclick="setStatus($.checkBoxValue('ids[]'), 1)" />
        &nbsp;&nbsp;
        <input type="button" value="取消审核" class="bginput" onclick="setStatus($.checkBoxValue('ids[]'), 0)" />
        &nbsp;&nbsp;
        <input type="button" value="批量删除" onclick="$.alerts.confirm('确认要删除? 删除后不可恢复!', null, function(r){ if(r){ delit($.checkBoxValue('ids[]')) } })" class="bginput"/>
</td>
    </tr>
</table>
</form>

    
<?php $this->display('footer.html');?>