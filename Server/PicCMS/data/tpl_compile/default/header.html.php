<?php
 /* compiled by (WeePHP) at (2014-08-14 13:55:00) */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $this->data['web_name'];
 if($this->data['title']){?> - <?php echo $this->data['title'];
 }
?> - Powered by <?php echo $this->data['sys_name'];?></title>
<meta name="keywords" content="<?php echo $this->data['web_keywords'];?>,<?php echo $this->data['cate']['ckeywords'];?>,<?php echo $this->data['title'];?>">
<meta name="description" content="<?php echo $this->data['web_description'];?>,<?php echo $this->data['cate']['cdescription'];?>,<?php echo $this->data['title'];?>">
<link rel="stylesheet" href="<?php echo $this->data['web_path'];?>images/default/piccms.css" media="screen" />
<script src="<?php echo $this->data['web_path'];?>images/js/jquery-1.4.4.min.js"></script>
<script src="<?php echo $this->data['web_path'];?>images/js/system.js"></script>
<script type="text/javascript">
var web_script = "<?php echo $this->data['web_script'];?>";
var web_url = "<?php echo $this->data['web_url'];?>";
var web_name = "<?php echo $this->data['web_name'];?>";
</script>
</head>

<body> 
<div class="wrap">
	<div class="header">
		<h1 class="logo"><a href="<?php echo $this->data['web_url'];?>"><span>PICCMS</span><img src="<?php echo $this->data['web_path'];?>images/logo.png" alt="<?php echo $this->data['web_name'];?>" /></a></h1>
		<div class="ad_top"><?php echo load_model('Tag')->adsense('banner');?></div>
		<!-- 导航 开始 -->
		<div class="nav">
			<b class="nav_left"></b>
			<div class="nav_bar">
				<ul>
					<li><a href="<?php echo $this->data['web_url'];?>"><span>首页</span></a></li>
                    <?php foreach($this->data['cateTree'] as $this->data['val']){
 if($this->data['val']['status']){?>
                        <li cid="<?php echo $this->data['val']['cid'];?>"><a href="<?php echo $this->data['val']['url'];?>" <?php if($this->data['cid']==$this->data['val']['cid']){?>class="current"<?php }
?>><span><?php echo $this->data['val']['name'];?></span></a></li>
                        <?php }

 }
?>
					<li><a href="http://www.piccms.com" target="_blank"><span>论坛</span></a></li>
				</ul>
                
                <!-- 下级分类 -->
				<?php if($this->data['cate']['parent']['son']){?>
                <div class="sub_nav">
                	<?php foreach($this->data['cate']['parent']['son'] as $this->data['val']){?>
                        <a href="<?php echo $this->data['val']['url'];?>"><?php echo $this->data['val']['name'];?></a>
                    <?php }
?> 
                </div>
                <?php } elseif($this->data['cate']['son']){?>
                <div class="sub_nav">
                	<?php foreach($this->data['cate']['son'] as $this->data['val']){?>
                        <a href="<?php echo $this->data['val']['url'];?>"><?php echo $this->data['val']['name'];?></a>
                    <?php }
?> 
                </div>
                <?php }
?>                        
                        
                
			</div>
			<div class="other_nav">
				<a href="<?php echo load_model('Tag')->rssurl();?>"><b class="icon icon_fav"></b>订阅RSS</a> <a href="javascript:alert('该功能尚未开启');"><b class="icon icon_send"></b>我要投稿</a>
			</div>
			<b class="nav_right"></b>
		</div>
		<!-- 导航 结束 -->
	</div>