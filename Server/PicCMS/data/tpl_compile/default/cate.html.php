<?php
 /* compiled by (WeePHP) at (2014-08-14 13:57:30) */

 $this->display('header.html');?>
<div class="container">
    <!-- 主要内容 开始 -->
    <div class="main">
        <!-- 图片列表 开始 -->
        <div class="box_2 pic_list_box">
            <div class="crumb">当前位置：
            	<a href="<?php echo $this->data['web_url'];?>">首页</a>
                &gt; 分类
                <?php if($this->data['cate']['parent']){?>
                &gt; <a href="<?php echo $this->data['cate']['parent']['url'];?>"><?php echo $this->data['cate']['parent']['name'];?></a>
                <?php }
?>
                &gt; <a href="<?php echo $this->data['cate']['url'];?>"><?php echo $this->data['cate']['name'];?></a>
            </div>
            <div class="pic_list_wrap">
                <ul>
                <?php foreach($this->data['list'] as $this->data['val']){?>
                    <li><a href="<?php echo $this->data['val']['url'];?>"><img src="<?php echo load_model('Tag')->image($this->data['val']['cover'], 130, 100);?>" alt="<?php echo $this->data['val']['title'];?>" /></a><a title="<?php echo $this->data['val']['title'];?>" href="<?php echo $this->data['val']['url'];?>"><?php echo Ext_String::cut($this->data['val']['title'], 10);?></a></li>
                <?php }
?>   
                </ul>
            </div>
            
            <div class="pager">
            	<span class="total">共<?php echo $this->data['totalNum'];?>条记录</span> 
                <?php echo $this->data['pageHtml'];?>
            </div>
        </div>
        <!-- 图片列表 结束 -->
    </div>
    <!-- 主要内容 结束 -->
    <!-- 侧边栏 开始 -->
    <div class="side">
        <div class="adv_side"><?php echo load_model('Tag')->adsense('cate-right');?></div>
        <!-- 每周热图 开始 -->
        <div class="box_2 week_hot_box week_hot_box_2">
            <div class="tit">
                <h3>点击排行</h3>
            </div>
            <div class="cont">
                <ol>
                <?php foreach(load_model('Tag')->article($this->data['cate']['cid'], 0, 10, 'hits') as $this->data['key'] => $this->data['val']){
 $this->data['i'] = $this->data['key'] + 1;?>
                    <li><span class="list_num num_<?php echo $this->data['i'];?>"><?php echo $this->data['i'];?></span><a href="<?php echo $this->data['val']['url'];?>" title="<?php echo $this->data['val']['title'];?>"><?php echo Ext_String::cut($this->data['val']['title'], 10);?></a></li>
                <?php }
?>
                </ol>
            </div>
        </div>
        <!-- 每周热图 结束 -->
        <!-- 搜索 开始 -->
        <div class="box_1 search_box">
            <div class="tit">
                <h3>搜索</h3>
            </div>
            <div class="cont">
                <input type="text" value="<?php echo $this->data['keyword'];?>" class="search_in" id="keyword" /> <button type="button" class="btn_normal btn_search" onclick="subsearch('keyword')">搜索</button>
            </div>
        </div>
        <!-- 搜索 结束 -->
        <!-- 热门图集 开始 -->
        <div class="box_2 hot_tag_box" style="height:212px;">
            <div class="tit">
                <h3>热门图集</h3>
            </div>
            <div class="cont" >
                <div class="tag_list">
                    <?php foreach(load_model('Tag')->tags(20) as $this->data['val']){?>
                    <a href="<?php echo $this->data['val']['url'];?>" class="tag_<?php echo $this->data['val']['star'];?>"><?php echo $this->data['val']['tag'];?></a>
                    <?php }
?>
                </div>
            </div>
        </div>
        <!-- 热门图集 开始 -->
    </div>
    <!-- 侧边栏 结束 -->
</div>
<?php $this->display('footer.html');?>