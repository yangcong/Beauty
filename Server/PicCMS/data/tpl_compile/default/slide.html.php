<?php
 /* compiled by (WeePHP) at (2014-08-14 14:00:19) */

 $this->display('header.html');?>

<script src="<?php echo $this->data['web_path'];?>images/js/article.js?id=12.30"></script>
<script type="text/javascript">
/*定义ID*/
var articleId = '<?php echo $this->data['article']['id'];?>';
/* 幻灯模式 */
viewBox.viewTotal = <?php echo $this->data['article']['attach_num'];?>;
viewBox.viewSize = 5;
viewBox.curPage = <?php echo $this->data['page'];?>;


/*文档就绪*/
$(document).ready(function() {
	loadInfo(articleId);
	<?php if($this->data['web_comment']){?>
	loadComment(articleId);
	<?php }
?>
	viewBox.autoShow();
});
</script>

<div class="container detail_container">
    <!-- 主要内容 开始 -->
    <div class="main">
        <!-- 详细内容 开始 -->
        <div class="box_2 detail_cont_box">
            <div class="crumb">当前位置：
            <a href="<?php echo $this->data['web_url'];?>">首页</a> 
            <?php if($this->data['cate']['parent']){?>
            &gt; <a href="<?php echo $this->data['cate']['parent']['url'];?>"><?php echo $this->data['cate']['parent']['name'];?></a>
            <?php }
?>
            &gt; <a href="<?php echo $this->data['cate']['url'];?>"><?php echo $this->data['cate']['name'];?></a>
            &gt; <a href="<?php echo $this->data['article']['url'];?>"><?php echo $this->data['article']['title'];?></a>
            </div>
            <div class="detail_cont">
                <h1><?php echo $this->data['article']['title'];?></h1>
                <div class="detail_info">
                <?php if($this->data['article']['author']){?>
                来源：
                    <?php if($this->data['article']['comeurl']){?>
                    <a href="<?php echo $this->data['article']['comeurl'];?>" target="_blank"><?php echo $this->data['article']['author'];?></a>
                    <?php } else{
 echo $this->data['article']['author'];
 }

 }
?>
                <span>更新时间：<?php echo $this->data['article']['pubdate'];?></span> <span>已经有<em id="p_c_hits_<?php echo $this->data['article']['id'];?>"><?php echo $this->data['article']['hits'];?></em>人关注</span></div>
            </div>
            
            <?php foreach($this->data['article']['attach'] as $this->data['key'] => $this->data['val']){?>
            <div class="detail_pic">
            	<a href="<?php echo $this->data['article']['next_url'];?>">
                	<img src="<?php echo load_model('Tag')->image($this->data['val']['file']);?>" alt="<?php echo $this->data['artilce']['title'];?>" title="<?php echo $this->data['val']['remark'];?>" />
                </a><br />
                <span><?php echo $this->data['val']['remark'];?></span>
            </div>
            <?php }
?>
            
            <div class="detail_pic_list">
                <ul>
                <?php foreach($this->data['article']['thumb'] as $this->data['key'] => $this->data['val']){?>
                    <li class="thumb-li" id="thumb-li-<?php echo $this->data['key'];?>"><a href="<?php echo $this->data['val']['article_url'];?>"><img src="<?php echo load_model('Tag')->image($this->data['val']['thumb_file']);?>" alt="<?php echo $this->data['article']['title'];?>" /></a></li>
                <?php }
?>
                </ul>
                <a href="javascript:viewBox.showPre()" class="prev_pic" title="上一张">上一张</a> <a href="javascript:viewBox.showNext()" class="next_pic" title="下一张">下一张</a>
            </div>
            
            <?php if($this->data['article']['content']){?>
            <div class="detail_txt">
                <?php echo $this->data['article']['content'];?>
            </div>
            <?php }

 if($this->data['article']['tagArr']){?>
            <div class="detail_tag">TAG：
                <?php foreach($this->data['article']['tagArr'] as $this->data['item']){?>
                <a href="<?php echo load_model('Tag')->searchurl($this->data['item']);?>"><?php echo $this->data['item'];?></a> 
                <?php }
?>
            </div>
            <?php }
?>
            
            <div class="detail_about">
            <?php if($this->data['article']['pre']){?>
            <a href="<?php echo $this->data['article']['pre']['url'];?>" class="prev_detail"><b class="icon icon_arrow_left"></b>上一篇：<?php echo $this->data['article']['pre']['title'];?></a> 
            <?php }

 if($this->data['article']['next']){?>
            <a href="<?php echo $this->data['article']['next']['url'];?>" class="next_detail">下一篇：<?php echo $this->data['article']['next']['title'];?><b class="icon icon_arrow_right"></b></a>
            <?php }
?>
            </div>
            <div class="detail_ctrl">
            <?php if($this->data['web_comment']){?>
            <a href="javascript:void(0)" onclick="window.location.href='#comment_tb'" class="goto_talk">我要发言</a> 
            <?php }
?>
            <a href="javascript:void(0)" onclick="loadInfo(<?php echo $this->data['article']['id'];?>, 'up')" class="goto_ding"><em id="p_c_up_<?php echo $this->data['article']['id'];?>"><?php echo $this->data['article']['up'];?></em><span>我顶</span></a> 
            <a href="javascript:void(0)" onclick="loadInfo(<?php echo $this->data['article']['id'];?>, 'down')" class="goto_down"><em id="p_c_down_<?php echo $this->data['article']['id'];?>"><?php echo $this->data['article']['down'];?></em><span>我踩</span></a> 
            <a href="javascript:void(0)" onclick="window.location.href='<?php echo $this->data['cate']['url'];?>'" class="goto_back">返回上级</a></div>
            
            <div class="detail_rela">
                <ul>
                <?php foreach(load_model('Tag')->article($this->data['article']['cid'], 0, 12) as $this->data['val']){?>
                    <li><a href="<?php echo $this->data['val']['url'];?>">·<?php echo Ext_String::cut($this->data['val']['title'], 12);?></a></li>
                <?php }
?>
                </ul>
            </div>
            
            <!--评论-->
            <?php if($this->data['web_comment']){?>
            <div id="comment_tb">
            </div>
            <?php }
?>
            
        </div>
        <!-- 详细内容 结束 -->
    </div>
    <!-- 主要内容 结束 -->
    <!-- 侧边栏 开始 -->
    <div class="side">
        <!-- 特别推荐 开始 -->
        <div class="box_2 commend_pic_box">
            <div class="tit">
                <h3>特别推荐</h3>
            </div>
            <div class="cont">
                <ol>
                <?php foreach(load_model('Tag')->article(0, "3,4,5", 10) as $this->data['key'] => $this->data['val']){
 $this->data['i'] = $this->data['key'] + 1;
 if($this->data['i'] < 5){?>
                    <li class="top_num<?php if(1 == $this->data['i']%2){?> top_num_1<?php }
?>">
                        <span class="list_num num_<?php echo $this->data['i'];?>"><?php echo $this->data['i'];?></span>
                        
                        <a href="<?php echo $this->data['val']['url'];?>" title="<?php echo $this->data['val']['title'];?>"><img src="<?php echo load_model('Tag')->image($this->data['val']['cover'], 120, 80);?>" alt="<?php echo $this->data['val']['title'];?>" /></a>
                        
                        <a href="<?php echo $this->data['val']['url'];?>" title="<?php echo $this->data['val']['title'];?>"><?php echo Ext_String::cut($this->data['val']['title'], 10);?></a>
                    </li>
                    <?php } else{?>
                    <li>
                        <span class="list_num num_<?php echo $this->data['i'];?>"><?php echo $this->data['i'];?></span>
                        <a href="<?php echo $this->data['val']['url'];?>" title="<?php echo $this->data['val']['title'];?>"><?php echo Ext_String::cut($this->data['val']['title'], 15);?></a>
                    </li>
                    <?php }

 }
?>
                </ol>
            </div>
        </div>
        <!-- 特别推荐 结束 -->
        <!-- 搜索 开始 -->
        <div class="box_1 search_box">
            <div class="tit">
                <h3>搜索</h3>
            </div>
            <div class="cont">
                <input type="text" value="<?php echo $this->data['keyword'];?>" class="search_in" id="keyword" /> <button type="button" class="btn_normal btn_search" onclick="subsearch('keyword')">搜索</button>
            </div>
        </div>
        <!-- 搜索 结束 -->
        <div class="adv_side"><?php echo load_model('Tag')->adsense('article-right');?></div>
        <!-- 频道精选 开始 -->
        <div class="box_2 nice_pic_box">
            <div class="tit">
                <h3>频道精选</h3>
            </div>
            <div class="cont">
                <ul>
                <?php foreach(load_model('Tag')->article($this->data['article']['cid'], 2, 4) as $this->data['val']){?> 
                    <li><a href="<?php echo $this->data['val']['url'];?>"><img src="<?php echo load_model('Tag')->image($this->data['val']['cover'], 120, 80);?>" alt="<?php echo $this->data['val']['title'];?>" /></a><a href="<?php echo $this->data['val']['url'];?>"><?php echo Ext_String::cut($this->data['val']['title'], 10);?></a></li>
                <?php }
?>
                </ul>
            </div>
        </div>
        <!-- 频道精选 结束 -->
        <!-- 人气排行 开始 -->
        <div class="box_2 week_hot_box person_love_box">
            <div class="tit">
                <h3>人气排行</h3>
            </div>
            <div class="cont">
                <ol>
                <?php foreach(load_model('Tag')->article(0, 0, 15, 'hits') as $this->data['key'] => $this->data['val']){
 $this->data['i'] = $this->data['key'] + 1;?>
                    <li><span class="list_num num_<?php echo $this->data['i'];?>"><?php echo $this->data['i'];?></span><a href="<?php echo $this->data['val']['url'];?>" title="<?php echo $this->data['val']['title'];?>"><?php echo Ext_String::cut($this->data['val']['title'], 15);?></a></li>
                <?php }
?>
                </ol>
            </div>
        </div>
        <!-- 人气排行 结束 -->
    </div>
    <!-- 侧边栏 结束 -->
</div>

<?php $this->display('footer.html');?>