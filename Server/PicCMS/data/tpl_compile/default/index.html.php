<?php
 /* compiled by (WeePHP) at (2014-08-14 13:55:00) */

 $this->display('header.html');?>

<script type="text/javascript">
/*页面初始化*/
$(document).ready(function() {
new weeFoucs({
    'delayTime': 3000,
    'target': '#ifocus_piclist li',
    'control': '#ifocus_btn li',
    'title': '#ifocus_tx li',
    'curClass': 'sel'
});
});
</script>
<div class="container">
    <!-- 主要内容 开始 -->
    <div class="main">
        <!-- 轮播图 开始 -->
        <div class="flash_pic">
            <div class="big_pic">
                <ul id="ifocus_piclist">
                <?php foreach(load_model('Tag')->article(0, 5, 6) as $this->data['val']){?>
                    <li style="display:none"><a href="<?php echo $this->data['val']['url'];?>"><img src="<?php echo load_model('Tag')->image($this->data['val']['cover'], 510, 360);?>" alt="<?php echo $this->data['val']['title'];?>" width="510" height="360"/></a></li>
                <?php }
?>
                </ul>
            </div>
            <div class="pic_list">
                <ul id="ifocus_btn">
                <?php foreach(load_model('Tag')->article(0, 5, 6) as $this->data['val']){?>
                    <li><a href="<?php echo $this->data['val']['url'];?>"><img width="50" height="50" src="<?php echo load_model('Tag')->image($this->data['val']['cover'], 50, 50);?>" alt="<?php echo $this->data['val']['title'];?>" /><strong><?php echo Ext_String::cut($this->data['val']['title'], 10);?></strong><span><?php echo Ext_String::cut($this->data['val']['des'], 20);?></span></a></li>
                <?php }
?>	
                </ul>
            </div>
        </div>
        <!-- 轮播图 结束 -->
        <!-- 精彩推荐 开始 -->
        <div class="box nice_pic_box">
            <div class="tit">
                <h3>精彩推荐</h3>
            </div>
            <div class="cont">
                <ul>
                    <?php foreach(load_model('Tag')->article(0, 4, 5) as $this->data['val']){?>
                    <li>
                        <a href="<?php echo $this->data['val']['url'];?>"><img src="<?php echo load_model('Tag')->image($this->data['val']['cover'], 100, 130);?>" alt="<?php echo $this->data['val']['title'];?>" width="100" height="130" /></a>
                        <p class="pic_name"><a href="javascript:void(0)"><?php echo Ext_String::cut($this->data['val']['title'], 10);?></a></p>
                        <p><?php echo Ext_String::cut($this->data['val']['des'], 20);?></p>
                    </li>
                    <?php }
?>
                </ul>
            </div>
            <a href="javascript:void(0)" class="more">更多&gt;&gt;</a>
        </div>
        <!-- 精彩推荐 结束 -->
    </div>
    <!-- 主要内容 结束 -->
    <!-- 侧边栏 开始 -->
    <div class="side">
        <!-- 最新图集 开始 -->
        <div class="box box_tab txt_list_tab">
            <div class="tit"><a href="javascript:void(0)" class="on" id="tab_1" onmouseover="$.showTab(1, 2)">最新图集</a> <a href="javascript:void(0)" id="tab_2" onmouseover="$.showTab(2, 2)">人气图集</a></div>
            <div class="cont" id="showtab_1">
                <ul>
                    <?php foreach(load_model('Tag')->article(0, 0, 9) as $this->data['val']){?>
                    <li><a href="<?php echo $this->data['val']['url'];?>" title="<?php echo $this->data['val']['title'];?>">·<?php echo Ext_String::cut($this->data['val']['title'], 10);?></a></li>
                    <?php }
?>
                </ul>
            </div>
            
            <div class="cont" id="showtab_2" style="display:none">
                <ul>
                    <?php foreach(load_model('Tag')->article(0, 0, 9, 'hits') as $this->data['val']){?>
                    <li><a href="<?php echo $this->data['val']['url'];?>" title="<?php echo $this->data['val']['title'];?>">·<?php echo Ext_String::cut($this->data['val']['title'], 10);?></a></li>
                    <?php }
?>
                </ul>
            </div>
        </div>
        <!-- 最新图集 结束 -->
        <!-- 搜索 开始 -->
        <div class="box_1 search_box">
            <div class="tit">
                <h3>搜索</h3>
            </div>
            <div class="cont">
                <input type="text" value="<?php echo $this->data['keyword'];?>" class="search_in" id="keyword" /> <button type="button" class="btn_normal btn_search" onclick="subsearch('keyword')">搜索</button>
            </div>
        </div>
        <!-- 搜索 结束 -->
        <!-- 每周热图 开始 -->
        <div class="box_1 week_hot_box">
            <div class="tit">
                <h3>每周热图</h3>
            </div>
            <div class="cont">
                <ol>
                    <?php foreach(load_model('Tag')->article(0, 0, 9, 'hits') as $this->data['key'] => $this->data['val']){
 $this->data['i'] = $this->data['key'] + 1;?>
                    <li><span class="list_num num_<?php echo $this->data['i'];?>"><?php echo $this->data['i'];?></span><a href="<?php echo $this->data['val']['url'];?>"><?php echo Ext_String::cut($this->data['val']['title'], 12);?></a></li>
                    <?php }
?>
                </ol>
            </div>
        </div>
        <!-- 每周热图 结束 -->
    </div>
    <!-- 侧边栏 结束 -->
    <div class="adv_contianer"><?php echo load_model('Tag')->adsense('index-top');?></div>
    <!-- 主要内容 开始 -->
    <div class="main">
        <div class="box_2 hot_pic_box pic_lib_box">
            <div class="tit">
                <h3>热门图集</h3>
            </div>
            <div class="cont">
                <div class="big_pic">
                <?php foreach(load_model('Tag')->article(0, 0, "0,1", 'up') as $this->data['val']){?>
                    <a href="<?php echo $this->data['val']['url'];?>"><img src="<?php echo load_model('Tag')->image($this->data['val']['cover'], 350, 270);?>" alt="<?php echo $this->data['val']['title'];?>" /></a><a title="<?php echo $this->data['val']['title'];?>" href="<?php echo $this->data['val']['url'];?>"><?php echo Ext_String::cut($this->data['val']['title'], 15);?></a>
                <?php }
?>
                </div>
                <div class="pic_list">
                    <ul>
                    <?php foreach(load_model('Tag')->article(0, 0, "1,2", 'up') as $this->data['val']){?>
                        <li><a href="<?php echo $this->data['val']['url'];?>"><img src="<?php echo load_model('Tag')->image($this->data['val']['cover'], 130, 100);?>" alt="<?php echo $this->data['val']['title'];?>" /></a><a title="<?php echo $this->data['val']['title'];?>" href="<?php echo $this->data['val']['url'];?>"><?php echo Ext_String::cut($this->data['val']['title'], 10);?></a></li>
                    <?php }
?>
                    </ul>
                </div>
                <div class="txt_list">
                    <ul>
                    <?php foreach(load_model('Tag')->article(0, 0, "3,10", 'up') as $this->data['val']){?>
                        <li><a title="<?php echo $this->data['val']['title'];?>" href="<?php echo $this->data['val']['url'];?>">·<?php echo Ext_String::cut($this->data['val']['title'], 15);?></a></li>
                    <?php }
?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- 主要内容 结束 -->
    <!-- 侧边栏 开始 -->
    <div class="side">
        <!-- 热门标签 开始 -->
        <div class="box_1 hot_tag_box">
            <div class="tit">
                <h3>热门标签</h3>
            </div>
            <div class="cont">
                <div class="tag_list">
                    <?php foreach(load_model('Tag')->tags(40) as $this->data['val']){?>
                    <a href="<?php echo $this->data['val']['url'];?>" class="tag_<?php echo $this->data['val']['star'];?>"><?php echo $this->data['val']['tag'];?></a>
                    <?php }
?> 
                </div>
            </div>
        </div>
        <!-- 热门标签 开始 -->
    </div>
    <!-- 侧边栏 结束 -->
    <!-- 高清大图 开始 -->
    <div class="box_2 change_pic_box">
        <div class="tit">
            <h3>高清大图</h3>
        </div>
        <div class="cont">
            <div class="change_pic">
                <ul id="change_div_1">
                <?php foreach(load_model('Tag')->article(0, 3, "0,5") as $this->data['val']){?>
                    <li><a href="<?php echo $this->data['val']['url'];?>"><img src="<?php echo load_model('Tag')->image($this->data['val']['cover'], 155, 120);?>" alt="<?php echo $this->data['val']['title'];?>" /></a><a title="<?php echo $this->data['val']['title'];?>" href="<?php echo $this->data['val']['url'];?>"><?php echo Ext_String::cut($this->data['val']['title'], 10);?></a></li>
                <?php }
?>
                </ul>
                
                <ul id="change_div_2" style="display:none;">
                <?php foreach(load_model('Tag')->article(0, 3, "5,5") as $this->data['val']){?>
                    <li><a href="<?php echo $this->data['val']['url'];?>"><img src="<?php echo load_model('Tag')->image($this->data['val']['cover'], 155, 120);?>" alt="<?php echo $this->data['val']['title'];?>" /></a><a title="<?php echo $this->data['val']['title'];?>" href="<?php echo $this->data['val']['url'];?>"><?php echo Ext_String::cut($this->data['val']['title'], 10);?></a></li>
                <?php }
?>
                </ul>
                <a href="javascript:changePic(1)" class="prev_pic">上一张</a> <a href="javascript:changePic(0)" class="next_pic">下一张</a>
                <script type="text/javascript">
				var thisId = 1;
				var maxId = 2;
				var idName = '#change_div_';
				function changePic(type) {
					if (type == 1) {
						thisId = thisId - 1;
						if (thisId <= 0) {
							thisId = maxId;	
						}
					} else {
						thisId = thisId + 1;
						if (thisId > maxId) {
							thisId = 1;	
						}
					}
					for (i = 1; i <= maxId; i++) {
						$(idName + i).hide();
					}
					$(idName + thisId).show();
				}
				</script>
            </div>
        </div>
        
    </div>
    <!-- 高清大图 结束 -->
    

    <!-- 分类图库 开始 -->
    <?php foreach($this->data['cateTree'] as $this->data['cate']){
 if($this->data['cate']['status']){?>
        <div class="box_2 pic_lib_box">
            <div class="tit">
                <h3><?php echo $this->data['cate']['name'];?></h3>
                <a href="<?php echo $this->data['cate']['url'];?>" class="more">更多&gt;&gt;</a>
            </div>
            <div class="cont">
                <div class="big_pic">
                <?php foreach(load_model('Tag')->article($this->data['cate']['cid'], 2, 1) as $this->data['val']){?>
                    <a href="<?php echo $this->data['val']['url'];?>"><img src="<?php echo load_model('Tag')->image($this->data['val']['cover'], 350, 270);?>" alt="<?php echo $this->data['val']['title'];?>" /></a><a href="<?php echo $this->data['val']['url'];?>" title="<?php echo $this->data['val']['title'];?>"><?php echo Ext_String::cut($this->data['val']['title'], 15);?></a>
                    <em class="pic_icon pic_hot_icon">HOT</em>
                <?php }
?>
                </div>
                <div class="pic_list">
                    <ul>
                    <?php foreach(load_model('Tag')->article($this->data['cate']['cid'], 1, 8) as $this->data['val']){?>
                        <li><a href="<?php echo $this->data['val']['url'];?>"><img src="<?php echo load_model('Tag')->image($this->data['val']['cover'], 130, 100);?>" alt="<?php echo $this->data['val']['title'];?>" /></a><a href="<?php echo $this->data['val']['url'];?>" title="<?php echo $this->data['val']['title'];?>"><?php echo Ext_String::cut($this->data['val']['title'], 10);?></a></li>
                    <?php }
?>
                    </ul>
                </div>
            </div>
        </div>
        <?php }

 }
?>
    <!-- 分类图库 结束 -->
    
    
    <div class="adv_contianer"><?php echo load_model('Tag')->adsense('index-bottom');?></div>
    <!-- 友情链接 开始 -->
    <div class="box f_link_box">
        <div class="tit">
            <h3>友情链接</h3>
        </div>
        <div class="cont">
            <div class="txt_link">
            <?php foreach(load_model('Tag')->Links(1) as $this->data['val']){?>
            <a href="<?php echo $this->data['val']['url'];?>" target="_blank"><?php echo $this->data['val']['title'];?></a>
            <?php }
?>
            </div>
            <div class="pic_link">
            <?php foreach(load_model('Tag')->Links(2) as $this->data['val']){?>
            <a href="<?php echo $this->data['val']['url'];?>" target="_blank" class="link_logo"><img src="<?php echo $this->data['val']['logo'];?>" title="<?php echo $this->data['val']['title'];?>" alt="<?php echo $this->data['val']['title'];?>" width="88" height="31" /></a>
            <?php }
?>
            </div>
        </div>
    </div>
    <!-- 友情链接 结束 -->
</div>

<?php $this->display('footer.html');?>