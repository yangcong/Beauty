/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50133
Source Host           : localhost:3306
Source Database       : piccms

Target Server Type    : MYSQL
Target Server Version : 50133
File Encoding         : 65001

Date: 2014-09-03 15:22:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `pc_admin`
-- ----------------------------
DROP TABLE IF EXISTS `pc_admin`;
CREATE TABLE `pc_admin` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UID',
  `name` varchar(30) NOT NULL COMMENT '用户名',
  `password` char(32) NOT NULL COMMENT '密码',
  `email` varchar(50) NOT NULL COMMENT '管理组',
  `pre` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uname` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pc_admin
-- ----------------------------
INSERT INTO `pc_admin` VALUES ('1', 'admin', '10b6b3d0887c48556c37914f110ffeca', '', '15');

-- ----------------------------
-- Table structure for `pc_adsense`
-- ----------------------------
DROP TABLE IF EXISTS `pc_adsense`;
CREATE TABLE `pc_adsense` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `des` varchar(50) NOT NULL DEFAULT '' COMMENT '说明',
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pc_adsense
-- ----------------------------

-- ----------------------------
-- Table structure for `pc_article`
-- ----------------------------
DROP TABLE IF EXISTS `pc_article`;
CREATE TABLE `pc_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` smallint(5) NOT NULL,
  `title` varchar(250) NOT NULL,
  `tag` varchar(100) NOT NULL DEFAULT '' COMMENT '标签',
  `color` char(8) NOT NULL DEFAULT '',
  `cover` varchar(250) NOT NULL DEFAULT '' COMMENT '封面',
  `author` varchar(50) NOT NULL DEFAULT '' COMMENT '作者',
  `comeurl` varchar(250) NOT NULL DEFAULT '' COMMENT '来源',
  `remark` text NOT NULL,
  `content` text NOT NULL,
  `hits` mediumint(8) NOT NULL DEFAULT '0',
  `star` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `up` mediumint(8) NOT NULL DEFAULT '0',
  `down` mediumint(8) NOT NULL DEFAULT '0',
  `jumpurl` varchar(255) NOT NULL DEFAULT '',
  `addtime` int(11) NOT NULL DEFAULT '0',
  `picfrom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `hits` (`hits`),
  KEY `star` (`star`),
  KEY `status` (`status`),
  KEY `up` (`up`),
  KEY `down` (`down`),
  KEY `addtime` (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pc_article
-- ----------------------------

-- ----------------------------
-- Table structure for `pc_attach`
-- ----------------------------
DROP TABLE IF EXISTS `pc_attach`;
CREATE TABLE `pc_attach` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '附件ID',
  `article_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '主题ID',
  `uid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '文件名',
  `remark` text NOT NULL COMMENT '文件描述',
  `size` int(11) NOT NULL DEFAULT '0' COMMENT '文件大小',
  `file` varchar(250) NOT NULL COMMENT '文件路径',
  `ext` varchar(10) NOT NULL COMMENT '文件类型',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态, 1:正常 0:隐藏',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '附件类型, 0:本地文件, 1:网络文件',
  `try_count` tinyint(2) NOT NULL DEFAULT '0' COMMENT '重试次数',
  `upload_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `picfrom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`),
  KEY `type` (`type`,`try_count`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pc_attach
-- ----------------------------

-- ----------------------------
-- Table structure for `pc_cache`
-- ----------------------------
DROP TABLE IF EXISTS `pc_cache`;
CREATE TABLE `pc_cache` (
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '字符串',
  `value` text NOT NULL COMMENT '显示文字',
  `package` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='公用数据存贮';

-- ----------------------------
-- Records of pc_cache
-- ----------------------------

-- ----------------------------
-- Table structure for `pc_cate`
-- ----------------------------
DROP TABLE IF EXISTS `pc_cate`;
CREATE TABLE `pc_cate` (
  `cid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `pid` smallint(5) NOT NULL,
  `oid` smallint(5) NOT NULL DEFAULT '0',
  `view_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '内容显示模式:0: 普通模式, 1:幻灯模式',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(30) NOT NULL DEFAULT '',
  `eng_name` varchar(30) NOT NULL DEFAULT '',
  `ctpl` varchar(30) NOT NULL DEFAULT '' COMMENT '分类模板',
  `ctitle` varchar(50) NOT NULL,
  `ckeywords` varchar(255) NOT NULL,
  `cdescription` varchar(255) NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pc_cate
-- ----------------------------

-- ----------------------------
-- Table structure for `pc_comment`
-- ----------------------------
DROP TABLE IF EXISTS `pc_comment`;
CREATE TABLE `pc_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(10) NOT NULL,
  `title` varchar(250) NOT NULL,
  `user_name` char(30) NOT NULL DEFAULT '' COMMENT '用户名',
  `up` int(11) NOT NULL DEFAULT '0',
  `ip` char(20) NOT NULL COMMENT 'IP',
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '默认状态',
  `dateline` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pc_comment
-- ----------------------------

-- ----------------------------
-- Table structure for `pc_config`
-- ----------------------------
DROP TABLE IF EXISTS `pc_config`;
CREATE TABLE `pc_config` (
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '字符串',
  `value` text NOT NULL COMMENT '显示文字',
  `des` varchar(50) NOT NULL DEFAULT '' COMMENT '说明',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='语言包(简体中文)';

-- ----------------------------
-- Records of pc_config
-- ----------------------------
INSERT INTO `pc_config` VALUES ('web_name', '美人册', '');
INSERT INTO `pc_config` VALUES ('web_url', 'http://localhost:88/PicCMS/', '');
INSERT INTO `pc_config` VALUES ('web_path', '/PicCMS/', '');
INSERT INTO `pc_config` VALUES ('web_email', '29500196@qq.com', '');
INSERT INTO `pc_config` VALUES ('web_qq', '29500196', '');
INSERT INTO `pc_config` VALUES ('web_icp', '沪ICP10001号', '');
INSERT INTO `pc_config` VALUES ('web_hotkey', '美女', '');
INSERT INTO `pc_config` VALUES ('web_keywords', '美女', '');
INSERT INTO `pc_config` VALUES ('web_description', '美女', '');
INSERT INTO `pc_config` VALUES ('web_copyright', 'Copyright © 2012 <strong style=\"color:#FF6600\">Manwo.Com</strong>, All Rights Reserved 美人册 版权所有', '');
INSERT INTO `pc_config` VALUES ('web_tongji', '', '');
INSERT INTO `pc_config` VALUES ('upload_http_thumb', '0', '');
INSERT INTO `pc_config` VALUES ('template_skin', 'default', '');
INSERT INTO `pc_config` VALUES ('web_admin_cover', '0', '');
INSERT INTO `pc_config` VALUES ('web_admin_pagenum', '10', '');
INSERT INTO `pc_config` VALUES ('web_list_pagenum', '25', '');
INSERT INTO `pc_config` VALUES ('web_maps_num', '100', '');
INSERT INTO `pc_config` VALUES ('web_rss_num', '100', '');
INSERT INTO `pc_config` VALUES ('web_comment', '0', '');
INSERT INTO `pc_config` VALUES ('web_comment_status', '0', '');
INSERT INTO `pc_config` VALUES ('web_comment_vcode', '0', '');
INSERT INTO `pc_config` VALUES ('web_comment_pagenum', '10', '');
INSERT INTO `pc_config` VALUES ('web_pick_hits', '100', '');
INSERT INTO `pc_config` VALUES ('web_pick_up', '100', '');
INSERT INTO `pc_config` VALUES ('web_adsensepath', '', '');
INSERT INTO `pc_config` VALUES ('upload_path', 'attach', '');
INSERT INTO `pc_config` VALUES ('upload_style', 'Y-m-d', '');
INSERT INTO `pc_config` VALUES ('upload_thumb_type', '0', '');
INSERT INTO `pc_config` VALUES ('upload_thumb_w', '9999', '');
INSERT INTO `pc_config` VALUES ('upload_thumb_h', '9999', '');
INSERT INTO `pc_config` VALUES ('upload_cut_pct', '80', '');
INSERT INTO `pc_config` VALUES ('upload_resize', '0', '');
INSERT INTO `pc_config` VALUES ('upload_max_w', '960', '');
INSERT INTO `pc_config` VALUES ('upload_max_h', '960', '');
INSERT INTO `pc_config` VALUES ('upload_water', '0', '');
INSERT INTO `pc_config` VALUES ('upload_water_img', 'images/water.png', '');
INSERT INTO `pc_config` VALUES ('upload_water_pct', '80', '');
INSERT INTO `pc_config` VALUES ('upload_water_pos', '9', '');
INSERT INTO `pc_config` VALUES ('upload_max_num', '99', '');
INSERT INTO `pc_config` VALUES ('upload_dispatch', '1', '');
INSERT INTO `pc_config` VALUES ('upload_safe_link', '0', '');
INSERT INTO `pc_config` VALUES ('upload_safe_domain', 'test.com|pic.com', '');
INSERT INTO `pc_config` VALUES ('upload_ftp', '0', '');
INSERT INTO `pc_config` VALUES ('upload_ftp_host', '', '');
INSERT INTO `pc_config` VALUES ('upload_ftp_user', '', '');
INSERT INTO `pc_config` VALUES ('upload_ftp_pass', '', '');
INSERT INTO `pc_config` VALUES ('upload_ftp_port', '', '');
INSERT INTO `pc_config` VALUES ('upload_ftp_dir', '', '');
INSERT INTO `pc_config` VALUES ('upload_ftp_url', '', '');
INSERT INTO `pc_config` VALUES ('url_mode', '1', '');
INSERT INTO `pc_config` VALUES ('url_suffix', '.html', '');
INSERT INTO `pc_config` VALUES ('url_html_index', '0', '');
INSERT INTO `pc_config` VALUES ('url_html_cate', '0', '');
INSERT INTO `pc_config` VALUES ('url_html_content', '0', '');
INSERT INTO `pc_config` VALUES ('url_html_maps', '0', '');
INSERT INTO `pc_config` VALUES ('url_dir_cate', 'html', '');
INSERT INTO `pc_config` VALUES ('url_dir_content', 'html/article', '');
INSERT INTO `pc_config` VALUES ('url_dir_maps', 'html/maps', '');
INSERT INTO `pc_config` VALUES ('url_create_time', '1', '');
INSERT INTO `pc_config` VALUES ('url_create_num', '10', '');
INSERT INTO `pc_config` VALUES ('html_cache_on', '0', '');
INSERT INTO `pc_config` VALUES ('html_cache_index', '60', '');
INSERT INTO `pc_config` VALUES ('html_cache_cate', '60', '');
INSERT INTO `pc_config` VALUES ('html_cache_content', '60', '');
INSERT INTO `pc_config` VALUES ('web_article_pagenum', '2', '');

-- ----------------------------
-- Table structure for `pc_lang_chs`
-- ----------------------------
DROP TABLE IF EXISTS `pc_lang_chs`;
CREATE TABLE `pc_lang_chs` (
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '字符串',
  `value` text NOT NULL COMMENT '显示文字',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='语言包(简体中文)';

-- ----------------------------
-- Records of pc_lang_chs
-- ----------------------------
INSERT INTO `pc_lang_chs` VALUES ('cms_name', 'Mypic图片管理系统');
INSERT INTO `pc_lang_chs` VALUES ('cms_ver', '3.0Beta');

-- ----------------------------
-- Table structure for `pc_link`
-- ----------------------------
DROP TABLE IF EXISTS `pc_link`;
CREATE TABLE `pc_link` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `oid` tinyint(3) NOT NULL,
  `type` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pc_link
-- ----------------------------

-- ----------------------------
-- Table structure for `pc_pick_list`
-- ----------------------------
DROP TABLE IF EXISTS `pc_pick_list`;
CREATE TABLE `pc_pick_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `url` varchar(250) NOT NULL,
  `article_id` int(11) NOT NULL DEFAULT '0',
  `is_picked` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否采集过',
  `p_content_urls` text NOT NULL COMMENT '分页地址',
  `p_content_page` mediumint(5) NOT NULL DEFAULT '0' COMMENT '当前页数',
  `do_time` int(11) NOT NULL DEFAULT '0' COMMENT '采集时间',
  PRIMARY KEY (`id`),
  KEY `rule_id` (`rule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pc_pick_list
-- ----------------------------

-- ----------------------------
-- Table structure for `pc_pick_rule`
-- ----------------------------
DROP TABLE IF EXISTS `pc_pick_rule`;
CREATE TABLE `pc_pick_rule` (
  `id` mediumint(7) NOT NULL AUTO_INCREMENT,
  `webname` varchar(100) NOT NULL DEFAULT '',
  `cid` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '文章状态',
  `charset_type` char(10) NOT NULL DEFAULT '' COMMENT '目标网站编码',
  `listurl` text NOT NULL,
  `page_begin` int(10) NOT NULL DEFAULT '0',
  `page_end` int(10) NOT NULL,
  `page_step` int(10) NOT NULL,
  `listmoreurl` text NOT NULL,
  `list_content_rule` text NOT NULL,
  `title_rule` text NOT NULL,
  `link_include_word` varchar(250) NOT NULL,
  `link_noinclude_word` varchar(250) NOT NULL,
  `link_replace_word` varchar(250) DEFAULT NULL,
  `title_replace_word` varchar(250) DEFAULT NULL,
  `content_rule` text NOT NULL,
  `file_rule` text NOT NULL,
  `file_include_word` varchar(250) NOT NULL,
  `file_noinclude_word` varchar(250) NOT NULL,
  `file_replace_word` varchar(250) DEFAULT NULL,
  `page_content_rule` text NOT NULL,
  `page_rule` text NOT NULL,
  `page_first` tinyint(1) NOT NULL DEFAULT '0' COMMENT '分页规则里是否包含有第1页',
  `last_pick_time` int(11) NOT NULL DEFAULT '0',
  `add_time` int(11) NOT NULL COMMENT '添加时间',
  `p_list_page` mediumint(5) NOT NULL DEFAULT '0' COMMENT '列表处理到多少页',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='采集规则';

-- ----------------------------
-- Records of pc_pick_rule
-- ----------------------------

-- ----------------------------
-- Table structure for `pc_tags`
-- ----------------------------
DROP TABLE IF EXISTS `pc_tags`;
CREATE TABLE `pc_tags` (
  `tag` varchar(30) NOT NULL,
  `title` varchar(250) NOT NULL COMMENT '文章标题',
  `article_id` int(11) NOT NULL COMMENT '文章ID',
  KEY `tag` (`tag`),
  KEY `article_id` (`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pc_tags
-- ----------------------------
