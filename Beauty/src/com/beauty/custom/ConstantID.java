package com.beauty.custom;

import java.util.List;

import com.beauty.model.CateModel;

public class ConstantID {

	public static final String HOST = "http://meirence.5520.pw";
	public static String PICPath = HOST + "/attach/";
	public static String URL_GetCateList = HOST + "/api/getcategory.php";
	public static String URL_GetarticleList = HOST + "/api/getalbum.php?";
	public static String URL_GetPicList = HOST + "/api/getpicture.php?";
	public static String URL_Version = HOST + "/api/version.xml";
	
	//类别存储
	public static List<CateModel> listCate;
}
