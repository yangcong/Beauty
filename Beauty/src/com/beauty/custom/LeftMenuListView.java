package com.beauty.custom;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;
/**
 * 自定义ListView
 *
 */
public class LeftMenuListView extends ListView {
	public LeftMenuListView(Context context) {
		super(context);
	}
	public LeftMenuListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public LeftMenuListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
				MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}

}
