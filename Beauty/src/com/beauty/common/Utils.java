package com.beauty.common;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;

public class Utils {
	/**
	 * 检测是否有可用网络
	 * 
	 */
	public static boolean checkNetWork(final Context context,boolean isShow) {
		boolean flag = false;
		ConnectivityManager cwjManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cwjManager.getActiveNetworkInfo() != null)
			flag = cwjManager.getActiveNetworkInfo().isAvailable();
		if (!flag&&isShow) {
			Builder b = new AlertDialog.Builder(context).setTitle("没有可用的网络")
					.setMessage("请开启GPRS或WIFI网络连接");
			b.setPositiveButton("确定", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					if (android.os.Build.VERSION.SDK_INT > 10) {
						context.startActivity(new Intent(
								android.provider.Settings.ACTION_SETTINGS));
					} else {
						context.startActivity(new Intent(
								android.provider.Settings.ACTION_WIRELESS_SETTINGS));
					}
				}
			}).setNeutralButton("取消", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.cancel();
				}
			}).create();
			b.show();
		}
		return flag;
	}
	
	
}
