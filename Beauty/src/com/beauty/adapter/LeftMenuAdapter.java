package com.beauty.adapter;

import java.util.List;

import com.beauty.model.CateModel;
import com.beauty.ui.AppManager;
import com.beauty.ui.Main;
import com.hk.beauty.R;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * 左侧菜单数据适配器
 */
public class LeftMenuAdapter extends BaseAdapter {

	private Context mContext;
	private List<CateModel> mLists;	
	private LayoutInflater mLayoutInflater;
	
	public LeftMenuAdapter(Context pContext,List<CateModel> pLists)
	{	
		this.mContext=pContext;
		this.mLists=pLists;
		mLayoutInflater=LayoutInflater.from(mContext);
	}
	@Override
	public int getCount() {
		return mLists!=null?mLists.size():0;
	}

	@Override
	public Object getItem(int arg0) {
		return mLists.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int arg0, View view, ViewGroup arg2) {
		Holder _Holder=null;;
		if(null==view){
			_Holder=new Holder();
			view=mLayoutInflater.inflate(R.layout.left_menu_item, null);
			_Holder.catename_item=(TextView)view.findViewById(R.id.catename);
			if(mLists.get(arg0).getCkeywords().equals("1")){
				_Holder.catename_item.setTextColor(Color.parseColor("#FF0000"));
			}
			view.setTag(_Holder);
			view.setOnClickListener(new ViewListener(mLists.get(arg0).getId(),mLists.get(arg0).getName()));
		}else {
			_Holder=(Holder)view.getTag();
		}
		_Holder.catename_item.setText(mLists.get(arg0).getName());
		return view;
	}
	
	class ViewListener implements OnClickListener{
		int catid;
		String _title;
		ViewListener(int id,String title){
			catid=id;
			_title=title;
		}
		@Override
		public void onClick(View arg0) {
			Intent intent = new Intent();   
			intent.setClass(mContext, Main.class);   //描述起点和目标   
			Bundle bundle = new Bundle();                           //创建Bundle对象   
			bundle.putString("catid",String.valueOf(catid));     //装入数据   
			bundle.putString("title",String.valueOf(_title));
			intent.putExtras(bundle);                                //把Bundle塞入Intent里面   
			mContext.startActivity(intent);                                     //开始切换 
		}
	}

	private class Holder
	{
		TextView catename_item;
	}
}
