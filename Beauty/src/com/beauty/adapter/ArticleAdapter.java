package com.beauty.adapter;

import java.io.File;
import java.util.LinkedList;
import java.util.WeakHashMap;

import com.beauty.common.AnimateFirstDisplayListener;
import com.beauty.common.AsyncImageLoad;
import com.beauty.common.Utils;
import com.beauty.custom.ConstantID;
import com.beauty.model.ArticleModel;
import com.beauty.ui.ImagePager;
import com.beauty.ui.Main;
import com.hk.beauty.R;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 数据适配器
 */
public class ArticleAdapter extends BaseAdapter {

	Context mContext;
	private LinkedList<ArticleModel> mInfos;
	private ViewHolder holder;
	private LayoutInflater mInflater;
	


	public class ViewHolder {
		ImageView imageView;
		TextView contentView;
	}

	public ArticleAdapter(Context context, LinkedList<ArticleModel> xListView) {
		
		mContext = context;
		mInfos = xListView;
		mInflater = LayoutInflater.from(context);
	}
	
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_piclist, null);
			holder = new ViewHolder();
			holder.imageView = (ImageView) convertView
					.findViewById(R.id.news_pic);
			holder.contentView = (TextView) convertView
					.findViewById(R.id.news_title);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		ArticleModel articleInfo = mInfos.get(position);
		if (articleInfo != null) {
			holder = (ViewHolder) convertView.getTag();
			holder.contentView.setText(articleInfo.getTitle());
			//new AsyncImageLoad(mContext, holder.imageView, articleInfo.getCover());
			
			holder.imageView.setScaleType(ScaleType.CENTER_CROP);
			Main.imageLoader.displayImage(articleInfo.getCover(),
					holder.imageView, AnimateFirstDisplayListener.getOptions(),
					new AnimateFirstDisplayListener());
			
			holder.imageView.setOnClickListener(new ImageListener(String
					.valueOf(articleInfo.getId()), String.valueOf(articleInfo
					.getTitle())));
			
		}
		
		return convertView;
	}

	class ImageListener implements OnClickListener {
		private String ablumid;
		private String title;

		ImageListener(String _ablumid, String _title) {
			ablumid = _ablumid;
			title = _title;
		}

		@Override
		public void onClick(View arg0) {
			if(Utils.checkNetWork(mContext, false)){
				Intent intent = new Intent();
				intent.setClass(mContext, ImagePager.class);
				Bundle bundle = new Bundle();
				bundle.putString("ablumid", ablumid);
				bundle.putString("title", title);
				intent.putExtras(bundle);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				mContext.startActivity(intent);
			}else{
				Toast.makeText(mContext, "主人，网没啦...",
					     Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public int getCount() {
		
		return mInfos.size();
	}

	@Override
	public Object getItem(int arg0) {
		return mInfos.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}
}
