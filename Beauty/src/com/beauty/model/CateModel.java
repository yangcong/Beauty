package com.beauty.model;

/**
 * 左侧菜单Model，对应数据库pc_cate
 * 
 */
public class CateModel {
	private Integer id; 
	private String name;
	private String ckeywords;
	
	
	public String getCkeywords() {
		return ckeywords;
	}
	public void setCkeywords(String ckeywords) {
		this.ckeywords = ckeywords;
	}

	public CateModel() {
		super();
	}

	public CateModel(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
