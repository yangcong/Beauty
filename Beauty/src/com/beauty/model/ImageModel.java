package com.beauty.model;

public class ImageModel {
	private Integer id;
	private String article_id;
	private String file;
	private String size;
	private Integer status;
	private String picfrom;
	
	
	public String getPicfrom() {
		return picfrom;
	}
	public void setPicfrom(String picfrom) {
		this.picfrom = picfrom;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getArticle_id() {
		return article_id;
	}
	public void setArticle_id(String article_id) {
		this.article_id = article_id;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}
