package com.beauty.model;

public class ArticleModel {
	private Integer id;
	private String cid;
	private String title;
	private String cover;
	private Integer hits;
	private String status;
	private String picfrom;

	public String getPicfrom() {
		return picfrom;
	}
	public void setPicfrom(String picfrom) {
		this.picfrom = picfrom;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCover() {
		return cover;
	}
	public void setCover(String cover) {
		this.cover = cover;
	}
	public Integer getHits() {
		return hits;
	}
	public void setHits(Integer hits) {
		this.hits = hits;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}	
