package com.beauty.api;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.beauty.custom.ConstantID;
import com.beauty.model.ArticleModel;
import com.beauty.model.CateModel;

import android.util.Log;

public class ApiClient {

	HttpPost post;
	List<NameValuePair> params = new ArrayList<NameValuePair>();
	private String server2client = "";
	private static ApiClient httpCt = new ApiClient();

	public static ApiClient getSingle() {
		return httpCt;
	}

	/**
	 * 访问网络，提交数据
	 * @param url
	 * @param params
	 * @return
	 */
	private String ServerToClient(String url, final List<NameValuePair> params) {
		post = new HttpPost(url);
		HttpParams httpParameters = new BasicHttpParams();
		try {
			if (params != null) {
				UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
						params, "utf-8");
				post.setEntity(urlEncodedFormEntity);
			}
			HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
			HttpClient httpClient = new DefaultHttpClient(httpParameters);
			HttpResponse re = httpClient.execute(post);
			if (re.getStatusLine().getStatusCode() == 200) {
				server2client = EntityUtils.toString(re.getEntity());
				return server2client.toString();
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.i("请求接口错误", e.toString());
			return null;
		} finally {
			server2client = null;
		}
	}

	/**
	 * 获取类别列表
	 * @return
	 */
	public List<CateModel> GetCateList(){
		String url=ConstantID.URL_GetCateList;
		Log.i("url", url);
		
		String result = ServerToClient(url, null);
		if (result != null) {
			JSONObject job;
			List<CateModel> lst=new ArrayList<CateModel>();
			try {
				job = new JSONObject(result);
				JSONArray jay = job.getJSONArray("rows");
				for (int i = 0; i < jay.length(); i += 1) {
					JSONObject temp = (JSONObject) jay.get(i);
					CateModel cart = new CateModel();
					cart.setId(Integer.valueOf(temp.getString("cid")));
					cart.setName(temp.getString("name"));
					cart.setCkeywords(temp.getString("ckeywords"));
					lst.add(cart);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			return lst;
		} else {
			return null;
		}
	}

	/**
	 * 分页获取专辑
	 * @return
	 */
	public LinkedList<ArticleModel> GetarticleList(int cartid,int minid){
		String url=ConstantID.URL_GetarticleList+"cateid="+cartid+"&minid="+minid;
		Log.i("pp", url);
		
		String result = ServerToClient(url, null);
		if (result != null) {
			JSONObject job;
			LinkedList<ArticleModel> lst=new LinkedList<ArticleModel>();
			try {
				job = new JSONObject(result);
				JSONArray jay = job.getJSONArray("rows");
				for (int i = 0; i < jay.length(); i += 1) {
					JSONObject temp = (JSONObject) jay.get(i);
					ArticleModel cart = new ArticleModel();
					cart.setId(Integer.valueOf(temp.getString("id")));
					cart.setCid(temp.getString("cid"));
					cart.setHits(Integer.valueOf(temp.getString("hits")));
					cart.setStatus(temp.getString("status"));
					cart.setTitle(temp.getString("title"));
					String picfrom= temp.getString("picfrom");
					String imgurl="";
					if(picfrom==null || picfrom.equals("null")||picfrom.equals("")){
						imgurl= ConstantID.PICPath +temp.getString("cover");
					}else{
						imgurl=temp.getString("cover");
					}
					cart.setCover(imgurl);
					lst.add(cart);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			return lst;
		} else {
			return null;
		}
	}
	
	/**
	 * 获取照片
	 * @return
	 */
	public LinkedList<String> GetPicList(String albumid){
		String url=ConstantID.URL_GetPicList+"albumid="+albumid;
		Log.i("url", url);
		
		String result = ServerToClient(url, null);
		if (result != null) {
			JSONObject job;
			LinkedList<String> lst=new LinkedList<String>();
			try {
				job = new JSONObject(result);
				JSONArray jay = job.getJSONArray("rows");
				for (int i = 0; i < jay.length(); i += 1) {
					JSONObject temp = (JSONObject) jay.get(i);
					String picfrom= temp.getString("picfrom");
					String imgurl="";
					if(picfrom==null || picfrom.equals("null")||picfrom.equals("")){
						imgurl= ConstantID.PICPath +temp.getString("file");
					}else{
						imgurl=temp.getString("file");
					}
					lst.add(imgurl);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			return lst;
		} else {
			return null;
		}
	}
}
