package com.beauty.ui;

import com.beauty.common.UpdateManager;
import com.hk.beauty.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Setting extends Activity {
	private LinearLayout update_version, leave_message, our_introduce;
	private ImageView btnback;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_layout);
		update_version=(LinearLayout) this.findViewById(R.id.update_version);
		leave_message=(LinearLayout) this.findViewById(R.id.leave_message);
		our_introduce=(LinearLayout) this.findViewById(R.id.our_introduce);
		btnback=(ImageView)this.findViewById(R.id.btn_back);
		
		update_version.setOnClickListener(listener);
		leave_message.setOnClickListener(listener);
		our_introduce.setOnClickListener(listener);
		btnback.setOnClickListener(listener);
	}

	View.OnClickListener listener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.update_version:
				// 检查软件更新
				UpdateManager manager = new UpdateManager(Setting.this);
				manager.checkUpdate();
				break;
			case R.id.leave_message:
				break;
			case R.id.our_introduce:
				Intent intent = new Intent();
				intent.setClass(getBaseContext(), About.class);
				startActivity(intent);
				break;
			case R.id.btn_back:
				back();
				break;
			}
		}

	};

	public void back() {
		this.finish();
	}
}
