package com.beauty.ui;

import java.util.ArrayList;
import java.util.List;

import s.d.f.AdManager;

import com.beauty.api.ApiClient;
import com.beauty.common.Utils;
import com.beauty.custom.ConstantID;
import com.beauty.model.CateModel;
import com.hk.beauty.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

/**
 * 应用程序启动类：显示欢迎界面并跳转到主界面
 */
public class AppStart extends Activity {

	private final int SPLASH_DISPLAY_LENGHT = 2000;
	List<CateModel> listCate=new ArrayList<CateModel>();
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(checkNetStart(this)){
			new Thread(new ReceiveThread()).start();
			
			new Handler().postDelayed(new Runnable() {  
	            public void run() {  
	                Intent mainIntent = new Intent(AppStart.this,  
	                		Main.class);  
	                AppStart.this.startActivity(mainIntent);  
	                AppStart.this.finish();  
	            }  
	        }, SPLASH_DISPLAY_LENGHT); 
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final View view = View.inflate(this, R.layout.welcome, null);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏

		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//去掉信息栏
		setContentView(view);
		
		AdManager.getInstance(this).setUserDataCollect(true);
	}
	
	class ReceiveThread implements Runnable {
		@Override
		public void run() {
			listCate=ApiClient.getSingle().GetCateList();
			handler.sendEmptyMessage(1);
		}
	}
	
	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (msg.what == 1) {
				ConstantID.listCate=listCate;
			}
		}
	};
	
	public void gotoMain(){
		Intent intent = new Intent(this, Main.class);
		startActivity(intent);
		finish();
	}
	
	public boolean checkNetStart(final Context context) {
		boolean isNet=false;
		ConnectivityManager cwjManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cwjManager.getActiveNetworkInfo() != null)
			isNet = cwjManager.getActiveNetworkInfo().isAvailable();
		if (!isNet) {
			Builder b = new AlertDialog.Builder(context).setTitle("没有可用的网络")
					.setMessage("请开启GPRS或WIFI网络连接");
			b.setPositiveButton("确定", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					if (android.os.Build.VERSION.SDK_INT > 10) {
						context.startActivity(new Intent(
								android.provider.Settings.ACTION_SETTINGS));
					} else {
						context.startActivity(new Intent(
								android.provider.Settings.ACTION_WIRELESS_SETTINGS));
					}
				}
			}).setNeutralButton("取消", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.cancel();
					finish();
				}
			}).create();
			b.show();
		}
		return isNet;
	}
}