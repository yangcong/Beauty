package com.beauty.ui;

import com.hk.beauty.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;

public class About extends Activity {
	private ImageView back;
	private WebView webView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
		findID();
	}

	private void findID() {

		back = (ImageView) this.findViewById(R.id.about_back);
		webView = (WebView) this.findViewById(R.id.about_webview);
		webView.loadUrl("file:///android_asset/about.html");
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				About.this.finish();
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == 4) {
			About.this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}

}
