package com.beauty.ui;

import java.util.ArrayList;
import java.util.List;

import com.beauty.adapter.LeftMenuAdapter;
import com.beauty.api.ApiClient;
import com.beauty.custom.ConstantID;
import com.beauty.model.CateModel;
import com.beauty.ui.AppStart.ReceiveThread;
import com.hk.beauty.R;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
/*
 * 左侧菜单
 */
public class LeftMenu extends Fragment{
	private View mView;
	private Context mContext;
	private ListView listview_common;
	ImageButton left_top_rigth;
	private ImageView nodata_IMG;
	List<CateModel> listCate=ConstantID.listCate;
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (null == mView) {
			mView = inflater.inflate(R.layout.left_menu_list, container,false);
			left_top_rigth=(ImageButton) mView.findViewById(R.id.left_top_rigth);
			left_top_rigth.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent();
					intent.setClass(mContext, Setting.class);
					mContext.startActivity(intent);
				}
			});
			
			initView();
			initValidata();
			bindData();
		}
		return mView;
	}
	
	
	
	/**
	 * 初始化界面元素
	 */
	private void initView() {
		listview_common = (ListView) mView.findViewById(R.id.listview_common);
		listview_common.setOnScrollListener(new PauseOnScrollListener(Main.imageLoader, false, false));
		nodata_IMG=(ImageView) mView.findViewById(R.id.nodata_leftMenu);
		listview_common.setEmptyView(nodata_IMG);
		nodata_IMG.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new Thread(new ReceiveThread()).start();
			}
		});
	}
	/**
	 * 初始化变量
	 */
	private void initValidata() {
		mContext = mView.getContext();
	}

	/**
	 * 绑定数据
	 */
	private void bindData() {
		listview_common.setAdapter(new LeftMenuAdapter(mContext, listCate));
	}
	
	//获取图片类别
	class ReceiveThread implements Runnable {
		@Override
		public void run() {
			listCate=ApiClient.getSingle().GetCateList();
			handler.sendEmptyMessage(1);
		}
	}
	
	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (msg.what == 1) {
				ConstantID.listCate=listCate;
				initValidata();
				bindData();
			}
		}
	};
}