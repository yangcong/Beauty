﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spider
{
    public class Album
    {
        public string AlbumImgHref { get; set; }
        public string AlbumName { get; set; }
        public string AlbumHref { get; set; }

        public List<string> ListPic { get; set; }
    }
}
