﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;

namespace Spider
{
    /// <summary>
    /// LogHelper封装类
    /// </summary>
    public static class SqlPrint
    {

        private static string serPath = AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "log\\" + DateTime.Now.ToString("yyyy-MM-dd");   //日志文件路径,事先指定              
        /// <summary>                                                
        /// 写日志                                                
        /// </summary>                                                                                             
        /// <param name="operateInfo">操作</param>                                                
        public static void WriteLog(string operateInfo, string type)
        {
            try
            {
                PathExists(serPath);
                string fname =serPath+"\\"+ type.ToString() + ".sql";   //指定日志文件的目录  
                FileInfo finfo = new FileInfo(fname);
                using (FileStream fs = finfo.OpenWrite())
                {
                    StreamWriter w = new StreamWriter(fs);
                    w.BaseStream.Seek(0, SeekOrigin.End);            //设置写数据流的起始位置为文件流的末尾  
                    w.Write("\r\n");
                    w.Write("" + operateInfo + "\r\n");
                    w.Write("\r\n");
                    w.Flush();                                      //清空缓冲区内容，并把缓冲区内容写入基础流           
                    w.Close();
                }
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
            }
        }

        /// <summary>
        /// 检查日志目录是否存在，如果不存在则建立文件夹
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static void PathExists(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

    }
}
